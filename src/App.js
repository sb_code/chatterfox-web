import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';

import withAuthentication from './app/Session/withAuthentication';
import Login from './app/views/pages/auth/Login';
import ModuleIndex from './app/views/pages/modules';
import ModuleDetails from './app/views/pages/modules/moduleDetails/moduleDetails.js';
import EditProfile from './app/views/pages/profile/editProfile.js';
import EditProfile2 from './app/views/pages/profile/editProfile2.js';
import ChangePass from './app/views/pages/profile/changePass.js';
import Settings from './app/views/pages/settings/settingsPage.js';
import BillingInfo from './app/views/pages/settings/billing/info.js';
import PurchaseHistory from './app/views/pages/settings/billing/purchaseHistory.js';
import UserActivity from './app/views/pages/settings/activity/userActivity.js';
import FluencyActivity from './app/views/pages/settings/activity/fluencyActivity.js';
import Invitation from './app/views/pages/invitation/invite.js';
//import Lessons from './app/views/pages/modules/lesson';

// import PreLesson from './app/views/pages/lesson/preLesson.js';
// import VideoLesson from './app/views/pages/lesson/videoLesson.js';
// import FinalLesson from './app/views/pages/lesson/finalLesson.js';
//import StartMessage from './app/views/pages/message/startMessage.js';
//import EndMessage from './app/views/pages/message/endMessage.js';
//import ChooseMessage from './app/views/pages/message/chooseMessage.js';



function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' name='Login' component={Login} />
        <Route path='/signin' name='Login' component={Login} />
       
        <Route
          path="/modules"
          render={({ match: { url } }) => (
            <>
              <Route path={`${url}/`} component={ModuleIndex}  exact />
              <Route path={`${url}/:moduleId`} component={ModuleIndex} exact/>
              <Route path={`${url}/:moduleId/:lessonName`} component={ModuleIndex} exact/>
            </>
          )}
        />

        <Route path='/editProfile' name='Edit Profile' component={EditProfile} />
        <Route path='/editProfileGift' name='Edit Profile' component={EditProfile2} />
        <Route path='/changePassword' name='Change Password' component={ChangePass} />
        
      

        
        <Route path='/settings' name='Settings' component={Settings} />
        <Route path='/billingInfo' name='Billing Info' component={BillingInfo} />
        <Route path='/purchases/:id' name='Purchase History' component={PurchaseHistory} />
        <Route path='/activity' name='Activities' component={UserActivity} />
        <Route path='/fluency' name='Fluency Activity' component={FluencyActivity} />
        <Route path='/invitation' name='Invite Friends' component={Invitation} />
       
       {/* <Route path='/preLesson' name='Pre Lesson' component={PreLesson} />
         <Route path='/videoLesson' name='Video Lesson' component={VideoLesson} />
         <Route path='/finalLesson' name='Final Lesson' component={FinalLesson} /> 

        <Route path='/startMessage' name='Start Lesson' component={StartMessage} />
        <Route path='/endMessage' name='End Lesson' component={EndMessage} />
        <Route path='/chooseMessage' name='Choose Lesson' component={ChooseMessage} />*/}
        
      </Switch>
    </BrowserRouter>
  );
}

export default withAuthentication(App);
