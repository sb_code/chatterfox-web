import React from 'react';
import {
  Router,
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
// import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './app/store';
import Login from './app/views/pages/auth/Login';
import Signup from './app/views/pages/auth/Signup';
import ForgotPassword from './app/views/pages/auth/ForgotPassword';
import EmailVerification from './app/views/pages/auth/EmailVerification';
import VerificationSuccess from './app/views/pages/auth/VerificationSuccess';
import ResetPassword from './app/views/pages/auth/ResetPassword';
import history from './app/util/history';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      {/* <App /> */}
      <Router history={history}>
        <Switch>
          <Route exact path='/' name='Sign In' component={Login} />
          <Route path='/signin' name='Sign In' component={Login} />
          <Route path='/signup' name='Sign Up' component={Signup} />
          <Route path='/reset-password' name='Reset Password' component={ResetPassword} />
          <Route path='/forgot-password' name='Forgot Password' component={ForgotPassword} />
          <Route path='/email-verification' name='Email Verification' component={EmailVerification} />
          <Route path='/verification-success' name='Verification Success' component={VerificationSuccess} />
          <Route component={App} />
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
