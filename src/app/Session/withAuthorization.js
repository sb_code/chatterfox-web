import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import AuthUserContext from './context';
import { SITE_CONTEXT } from '../constants/global';
import { getData } from '../util/localStorageHandler';
// import * as ROUTES from '../../constants/routes';

const withAuthorization = condition => Component => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      let authUser = getData();
      // console.log('authUser --- -- ', authUser, authUser.api_token, authUser.user.email, authUser.user.id);
      // change here
      if (authUser && authUser.api_token && authUser.user.email && authUser.user.id) {
        if (!condition(authUser)) {
          console.log(authUser);
          this.props.history.push(`${SITE_CONTEXT}/`);
        } else {
          // console.log('here1');
        }
      } else {
        // console.log('here3');
        this.props.history.push(`${SITE_CONTEXT}/signin`);
      }
    }

    render() {
      return (
        <AuthUserContext.Consumer>
          {authUser =>
            condition(authUser) ? <Component {...this.props} /> : null
          }
        </AuthUserContext.Consumer>
      );
    }
  }

  return compose(
    withRouter,
  )(WithAuthorization);
};

export default withAuthorization;