import { createBrowserHistory } from 'history';
// https://stackoverflow.com/questions/44153517/how-to-access-history-object-outside-of-a-react-component

const history = createBrowserHistory();
export default history;