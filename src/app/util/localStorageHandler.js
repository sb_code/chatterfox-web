import { decryptData, encryptData } from "./cryptoConverter";

export function setData(data) {
  try {
    if (data) {
      localStorage.setItem("CtF_Local", encryptData(JSON.stringify(data)));
    } else {
      localStorage.setItem("CtF_Local", JSON.stringify(data));
    }
    return true
  } catch (error) {
    return null
  }
}

export function getData() {
  try {
    let storage = localStorage.getItem("CtF_Local");
    storage = storage ? JSON.parse(decryptData(storage)) : null;
    // console.log('storage', storage);
    return storage;
  } catch (error) {
    return null
  }
}