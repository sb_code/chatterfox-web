import { logout } from "../actions/user/userSlice";
import { API_BASEURL, CF_USER } from "../constants/global";
import { postFileData } from "../util/httpClient";
import { getData } from "../util/localStorageHandler";

export async function submitUserRecordedData(dataToSend,authToken) {
  return new Promise(async (resolve, reject) => {
    let url = `${API_BASEURL}/module/upload-record`;
    try {
      // Upload user recorded file
      let response = await postFileData(url, dataToSend,authToken);
      console.log(response);
      if (response && !response.isError) {
        resolve(response.data);
      } else {
        reject({ message: response && response.message ? response.message : 'Some error occurred. Please try again.' });
      }
    } catch (e) {
      reject({ message: "Some error occurred. Please try again." });
    }
  });
}

