import { logout } from "../actions/user/userSlice";
import { API_BASEURL, CF_USER } from "../constants/global";
import { postFileData, post, get } from "../util/httpClient";
import { getData } from "../util/localStorageHandler";

export const getModuleList = async (token) => {
   
    let path = `${API_BASEURL}/module/list`;
    return get(path, token) 
   
}

export const getModuleDetailService = async (token,id) => {
   
    let path = `${API_BASEURL}/module/detail`;
    let data = {"module_id":id};
    return post(path,data, token); 
   
}