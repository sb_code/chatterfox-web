import { createSlice } from '@reduxjs/toolkit';
import { setData } from '../../util/localStorageHandler';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    "api_token": "",
    "id": "",
    "name": "",
    "is_verified": false,
    "email": "",
    "mb_no": "",
    "photo_url": "",
    "username": "",
    "role_id": "",
    "searchKey": "",
    "searching": false,
    "menu": "",
  },

  reducers: {
    setUser: (state, action) => {
      state.api_token = action.payload.api_token;
      state.id = action.payload.user.id;
      state.name = action.payload.user.name;
      state.is_verified = action.payload.user.is_verified;
      state.email = action.payload.user.email;
      state.mb_no = action.payload.user.mb_no;
      state.photo_url = action.payload.user.photo_url;
      state.username = action.payload.user.username;
      state.role_id = action.payload.user.role_id;
      state.refferal_coupon = action.payload.user.refferal_coupon;

      // Set local storage
      let thisUser = {}
      thisUser.id = action.payload.user.id;
      thisUser.name = action.payload.user.name;
      thisUser.is_verified = action.payload.user.is_verified;
      thisUser.email = action.payload.user.email;
      thisUser.mb_no = action.payload.user.mb_no;
      thisUser.photo_url = action.payload.user.photo_url;
      thisUser.username = action.payload.user.username;
      thisUser.role_id = action.payload.user.role_id;
      thisUser.refferal_coupon = action.payload.user.refferal_coupon;
      setData({
        api_token: action.payload.api_token,
        user: thisUser,
      });
    },

    setSearch: (state, action) => {
      state.searchKey = action.payload.searchKey;
      state.searching = action.payload.searching;
    },

    setMenu: (state, action)=> {
      state.menu = action.payload;
    },

    logout: (state) => {
      state.api_token = "";
      state.id = "";
      state.name = "";
      state.is_verified = "";
      state.email = "";
      state.mb_no = "";
      state.photo_url = "";
      state.username = "";
      state.role_id = "";

      // Clear local storage
      setData("");
    },
  },
});

export const { setUser, setSearch, setMenu, logout } = userSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(setUserAsync(user))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
export const setUserAsync = user => dispatch => {
  setTimeout(() => {
    dispatch(setUser(user));
  }, 1000);
};

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.user.value)`
export const selectUser = state => state.user;

export default userSlice.reducer;

/**
 * https://redux-toolkit.js.org/api/createslice
 */