import { createSlice } from '@reduxjs/toolkit';
import { setData } from '../../util/localStorageHandler';

export const lessonStore = createSlice({
  name: 'lessonstore',
  initialState: {
    "data": {}
  },

  reducers: {
    setLessonStore: (state, action) => {

      console.log("lessonstore action..",action.payload);
      state.data = action.payload;

    },

  }
});

//this is distach action function
export const { setLessonStore } = lessonStore.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(setBreadCrumpsPathAsync(user))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
export const setLessonStoreAsync = lessonstore => dispatch => {
  setTimeout(() => {
    dispatch(setLessonStore(lessonstore));
  }, 1000);
};

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.user.value)`
export const selectLessonStore = state => state.lessonstore;

export default lessonStore.reducer;

/**
 * https://redux-toolkit.js.org/api/createslice
 */