import React, { useEffect, useState } from 'react';

import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { selectUser } from '../../../actions/user/userSlice';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../constants/global';
import { get, post } from '../../../util/httpClient';
import { setData, getData } from '../../../util/localStorageHandler.js';
import withAuthorization from '../../../Session/withAuthorization';
import Swal from 'sweetalert2';

import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';

const ChangePass = props => {

    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const history = useHistory();

    const [authToken, setAuthToken] = useState("");
    const [oldPass, setOldPass] = useState("");
    const [newPass, setNewPass] = useState("");
    const [cnfPass, setCnfPass] = useState("");

    useEffect(()=> {
        // console.log("store datas are === ", user);
        let data = getData();
        if (data) {
            // setAuthToken(user.api_token);
            setAuthToken(data.api_token);
        }
    }, []);


    const changePass= async(e)=>{
        e.preventDefault();
        let dataToSend = {
            "new_password": newPass,
            "old_password": oldPass,
            "new_password_confirmation": cnfPass
        };
        
        let path = `${API_BASEURL}/update/password`;
        let res = await post(path, dataToSend, authToken);
        // console.log("change password response === ", res);
        if(res && !res.isError){
            setOldPass();
            setNewPass();
            setCnfPass();
            Swal.fire('Success!', "Successfully Updated.", 'success');
            history.push({
                pathname: "/editProfileGift",
                state: {}
            })
        }else{
            res && Swal.fire('Error!', res.message, 'error');
        }

    }

    return (
        <>
            <Header title="Change Password"/>
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="profile extra-padding pt-0">
                        <div class="container">
                            <div class="profile-form-edit-sec">
                                <form>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" class="form-control" 
                                                placeholder="Enter your Old Password" 
                                                value={oldPass}
                                                onChange={(e)=>setOldPass(e.target.value)}
                                            />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" class="form-control" 
                                                placeholder="Enter your New Password" 
                                                value={newPass}
                                                onChange={(e)=>setNewPass(e.target.value)}
                                            />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" class="form-control" 
                                                placeholder="Confirm Password" 
                                                value={cnfPass}
                                                onChange={(e)=>setCnfPass(e.target.value)}
                                            />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn-red btn-orange" onClick={(e)=>changePass(e)}>
                                                Change Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </>
    )

}


const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(ChangePass);
