import React, { useEffect, useState, useRef } from 'react';
import { Modal, Button } from 'react-bootstrap';
import axios from "axios";
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';

import { selectUser, setUser } from '../../../actions/user/userSlice';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../constants/global';
import { get, post } from '../../../util/httpClient';
import { setData, getData } from '../../../util/localStorageHandler.js';
import withAuthorization from '../../../Session/withAuthorization';

import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';


const Editprofile2 = props => {

    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const history = useHistory();

    const fileRef = useRef(null);

    const [authToken, setAuthToken] = useState("");
    const [email, setEmail] = useState("");
    const [userId, setUserId] = useState("");
    const [userData, setUserData] = useState({});
    const [countries, setCountries] = useState([]);
    const [languageList, setLanguageList] = useState([{ name: "Chinese" }, { name: "Spanish" }, { name: "Hindi" }, { name: "Persian" }, { name: "Arabic" }, { name: "Portuguese" },
    { name: "Bengali" }, { name: "Russian" }, { name: "Japanese" }, { name: "Punjabi" }, { name: "German" }, { name: "Javanese" },
    { name: "Malay/Indonesian" }, { name: "Korean" }, { name: "Telugu" }, { name: "Vietnamese" }, { name: "French" }, { name: "Marathi" },
    { name: "Tamil" }, { name: "Urdu" }, { name: "Turkish" }, { name: "Italian" }, { name: "Thai" }, { name: "Polish" }, { name: "Others" }]);

    const [ages, setAges] = useState(["6-10", "11-15", "16-20", "21-25", "26-30", "31-35", "36-40", "41-45", "46-50", "51-55", "56-60", "61-65"]);

    const [name, setName] = useState("");
    const [country, setCountry] = useState("");
    const [gender, setGender] = useState("");
    const [age, setAge] = useState("");
    const [language, setLanguage] = useState("");
    // const [englishExp, setEnglishExp] = useState("");

    const [edit, setEdit] = useState(false);
    const [show, setShow] = useState(false);
    const [code, setCode] = useState("");


    useEffect(() => {
        let data = getData();
        if (data) {
            getUserData(data.api_token);
            setAuthToken(data.api_token);
            // setAuthToken(user.api_token);
        }
        getCountryList();
    }, [])


    const getCountryList = async () => {

        let path = `${API_BASEURL}/countries`;
        let res = await get(path);

        // console.log("All country list ===", res.data);
        if (res && !res.isError) {
            res.data && setCountries(res.data);
        }

    }

    const getUserData = async (data) => {

        let path = `${API_BASEURL}/get-user-data`;
        let res = await get(path, data);
        // console.log("response of user-data ==", res);
        if (res && !res.isError) {
            if (res.userDetails) {
                let details = res.userDetails;
                details && setUserData(details);
                details && details.email ? setEmail(details.email) : setEmail("");
                details && details.name ? setName(details.name) : setName("");
                details && details.country ? setCountry(details.country) : setCountry("");
                details && details.gender ? setGender(details.gender) : setGender("");
                details && details.birthday ? setAge(details.birthday) : setAge("");
                details && details.native_language ? setLanguage(details.native_language) : setLanguage("");
                // details && details.english_teaching_exp && setEnglishExp(details.english_teaching_exp);
            }
        }

    }

    const onFileChange = async (e) => {

        e.preventDefault();
        // console.log("input file data ===", e.target.files[0].name);
        // let file = fileRef;
        let file = e.target.files[0];

        if (file) {
            let blnValid = false;
            let sFileName = file.name;
            var _validFileExtensions = [".jpg", ".jpeg", ".png"];
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                Swal.fire('Error!', "File type is not valid.", 'error');
            } else {

                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                // reader.onloadend = function (e) {
                //     this.setState({
                //         loading: true,
                //         imgUrl: [reader.result]
                //     })
                // }.bind(this);

                const photo = new FormData();
                photo.append('image', file);
                // photo.append('companyId', this.state.companyId);

                let headers = {
                    'Content-Type': 'multipart/form-data'
                }
                if (authToken) headers['Authorization'] = `Bearer ${authToken}`;
                console.log("header to be sent ===", headers);
                
                axios
                    .post(API_BASEURL + "/upload-avatar", photo, { headers })
                    .then(serverResponse => {
                        console.log("uploading response is ===", serverResponse);
                        let res = serverResponse.data;
                        if(res && !res.isError){
                            Swal.fire('Success!', "Successfully Uploaded.", 'success');
                            getUserData(authToken);
                            if(res.userDetails){
                                setData(res.userDetails);
                                dispatch(setUser(res.userDetails));
                            }
                        }else{
                            res && Swal.fire('Error!', "Sorry! something went wrong.", 'error');
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })


            }

        }

    }

    const updateProfile = async (event) => {
        event.preventDefault();
        if (edit) {
            // let dataToSend = {
            //     "email": email,
            //     "role": CF_USER,
            //     "family_name": '',
            //     "mb_no": 
            //     "gender":
            //     "about":
            //     "native_language": 
            //     "country":
            //     "city": 
            //     "occupation":
            //     "education_level":
            //     "certificate": 
            // };

            let dataToSend = {
                "email": email,
                "role": CF_USER,
                "name": name,
                "username": name,
                "birthday": age,
                "gender": gender,
                "native_language": language,
                "country": country
            }

            let path = `${API_BASEURL}/update-profile`;
            let res = await post(path, dataToSend, authToken);

            if (res && !res.isError) {
                Swal.fire('Success!', "Successfully Updated.", 'success');
                getUserData(authToken);
                if(res.userDetails){
                    setData(res.userDetails);
                    dispatch(setUser(res.userDetails));
                }
                setEdit(false);
            } else {
                res && Swal.fire('Error!', res.message, 'error');
            }
        }

    }

    const changePassword = (event) => {
        event.preventDefault();
        history.push({
            pathname: "/changePassword",
            state: {}
        })

    }

    const giftCard = () => {
        setShow(true);
    }

    const handleClose = () => {
        setShow(false);
        setCode("");
    }

    const handleSubmit = async () => {

        let dataToSend = {
            "coupon": code
        }

        let path = `${API_BASEURL}/apply-coupon`;
        let res = await post(path, dataToSend, authToken);

        if (res && !res.isError) {
            Swal.fire('Success!', "Successfully Updated.", 'success');
            getUserData(authToken);
            handleClose();
            getUserData(authToken);
        } else {
            res && Swal.fire('Error!', res.message, 'error');
            handleClose();
        }


    }


    return (
        <>
            <Header title="Edit Profile" />
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="profile extra-padding">
                        <div class="container">
                            <div class="profile-bg">
                                <div class="figure">
                                    {/* <img src="images/profile-ic.png" /> */}
                                    {userData.photo_url ? <img src={userData.photo_url} /> : <img src="images/profile-ic.png" />}
                                    <input type="file" ref={fileRef} name="profilePic" onChange={(e) => { onFileChange(e) }} />
                                </div>
                                <div class="figcaption">
                                    <h3>{userData.email && userData.email}</h3>
                                    <p>Your bonus balance: {userData.online_credit}$</p>
                                </div>
                            </div>
                            <button type="submit" class="btn-red btn-orange d-block m-auto" onClick={() => giftCard()}>Gift Card</button>
                        </div>
                    </section>
                </div>

                <section class="profile-edit extra-padding">
                    <div class="container">
                        <div class="profile-form-edit-sec">
                            <form>
                                {!edit ?
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="JavaScript:Void(0);" class="text-center d-block mb-2" onClick={() => setEdit(true)}>
                                                <i class="fa fa-pencil"></i> Edit Your Profile
                                        </a>
                                        </div>
                                    </div>
                                    : null}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" placeholder="Enter your name *"
                                            value={name} onChange={(e) => setName(e.target.value)}
                                            readOnly={edit ? false : true}
                                        />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <select class="form-control" id="" onChange={(e) => setCountry(e.target.value)} disabled={edit ? false : true}>
                                            <option value="">Country of your current Resident</option>
                                            {countries.map((data, index) => {
                                                return <option value={data.name} selected={data.name == country} key={index}>
                                                    {data.name}
                                                </option>
                                            })}
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 pl-2">
                                        <label class="d-block">Gender :</label>
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio1">
                                                <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1"
                                                    checked={gender == "male" ? true : false} onClick={() => setGender("male")} disabled={edit ? false : true} />
                                                Male
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio2">
                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2"
                                                    checked={gender == "female" ? true : false} onClick={() => setGender("female")} disabled={edit ? false : true} />
                                                Female
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio3">
                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option2"
                                                    checked={gender == "other" ? true : false} onClick={() => setGender("other")} disabled={edit ? false : true} />
                                                Others
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <select class="form-control" id="" onChange={(e) => setAge(e.target.value)} disabled={edit ? false : true}>
                                            <option value="">Select Age</option>
                                            {ages && ages.map((data, index) => {
                                                return <option selected={data == age ? true : false} value={data} key={index}>
                                                    {data}
                                                </option>
                                            })}
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <select class="form-control" id="" onChange={(e) => setLanguage(e.target.value)} disabled={edit ? false : true}>
                                            <option value="" selected>Select Native Language</option>
                                            {languageList && languageList.map((data, index) => {
                                                return <option value={data.name} selected={data.name == language ? true : false} key={index}>
                                                    {data.name}
                                                </option>
                                            })}
                                        </select>
                                    </div>
                                </div>

                                {/* <div class="row">
                                    <div class="col-lg-12">
                                        <select class="form-control" id="">
                                            <option>Years of English Experience</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div> */}

                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn-red" onClick={(e) => updateProfile(e)} >Update Profile</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn-red btn-orange mt-0" onClick={(e) => changePassword(e)} >Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Enter the code</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <input
                        type="text"
                        class="form-control"
                        placeholder="Enter code"
                        value={code}
                        onChange={e => setCode(e.target.value)}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>
                    <Button variant="primary" onClick={handleSubmit}>Redeem Now</Button>
                </Modal.Footer>
            </Modal>


        </>
    )

}

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(Editprofile2);