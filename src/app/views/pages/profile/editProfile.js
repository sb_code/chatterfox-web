import React, { useEffect, useState } from 'react';

import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { selectUser } from '../../../actions/user/userSlice';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../constants/global';
import { get, post } from '../../../util/httpClient';
import { setData, getData } from '../../../util/localStorageHandler.js';

import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';


const EditProfile = props => {

    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const history = useHistory();

    const [authToken, setAuthToken] = useState("");
    const [email, setEmail] = useState("");
    const [userId, setUserId] = useState("");
    const [userData, setUserData] = useState({});

    useEffect(()=>{
        let data = getData();
        if(data){
            setAuthToken(user.api_token);
        }
        getUserData(user.api_token);
        console.log("Token is ===", user.api_token);
    },[])

    const getUserData= async(data)=> {

        let path = `${API_BASEURL}/get-user-data`;
        let res = await get(path, data);
        // console.log("response of user-data ==", res);
        if(res && !res.isError){
            if(res.userDetails){
                setUserData(res.userDetails);

            }
        }else{
            setUserData({});
        }

    }

    return (
        <>
            <Header title="Edit Profile" />
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="profile extra-padding">
                        <div class="container">
                            <div class="profile-bg">
                                <div class="figure">
                                    <img src="images/profile-ic.png" />
                                </div>
                                <div class="pro-pic">
                                    <img src="images/camera.png" />
                                </div>
                                <div class="figcaption">
                                    <h3>loremipsum@example.com</h3>
                                    <p>Your bonus balance: 80.50$</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <section class="profile-edit extra-padding">
                    <div class="container">
                        <div class="profile-form-edit-sec">
                            <form>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Full Name :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" placeholder="Enter your name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Country of your current Resident :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="">
                                            <option>Select</option>
                                            <option>New York, USA</option>
                                            <option>New York, USA 2</option>
                                            <option>New York, USA 3</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Gender :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio1"> <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" checked />Male </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio2"> <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2" />Female </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label" for="radio3"> <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option2" />Others </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Age :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="">
                                            <option>Select</option>
                                            <option>30</option>
                                            <option>31</option>
                                            <option>32</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Native Language :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="">
                                            <option>Select</option>
                                            <option>Spanish</option>
                                            <option>English</option>
                                            <option>Hindi</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Years of English Experience :</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="">
                                            <option>Select</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn-red">Update Profile</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn-red btn-orange mt-0">Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>

        </>
    )

}

export default EditProfile;