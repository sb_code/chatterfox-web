import React, { useEffect, useState } from 'react';
import { getData } from '../../../util/localStorageHandler.js';
import ErrorTextInline from '../../components/ErrorTextInline.js';


import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';

const Invitation = porps => {
    const [refferal_coupon, setRefferalCoupon] = useState("");
    const [couponAlert, setCouponAlert] = useState("");

    useEffect(() => {
        let data = getData();
        if (data) {
            console.log("store datas are === ", data);
            // setAuthToken(user.api_token);
            setRefferalCoupon(data.user.refferal_coupon);
        }
    }, []);

    const copyToClipbord = () => {
        if (refferal_coupon) {
            navigator.clipboard.writeText(refferal_coupon);
            showCouponAlert('Code copied.');
        } else {
            showCouponAlert('No code found.');
        }
    }

    const showCouponAlert = (alertText) => {
        setCouponAlert(alertText);
        setTimeout(() => {
            setCouponAlert("");
        }, 2000);
    }

    return (
        <>
            <Header title="Invitation" />
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper bg-invite">
                    <section class="invite-friends">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="figure">
                                        <img src="images/left.png" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="figcaption text-center">
                                        <strong>Learn with your friends+</strong>
                                        <h1>$5 for you, $5 for your friend</h1>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="figure">
                                        <img src="images/right.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <section class="body-mid-invite sec-pad">
                    <div class="container">
                        <figcaption class="text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. t enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <strong class="d-block">Your Code</strong>
                            {refferal_coupon ? <button class="btn btn-primary copy-code d-block"> {refferal_coupon}</button> :
                                <button class="btn btn-primary copy-code d-block">NO CODE</button>}
                            {refferal_coupon ? <button class="btn d-block submit" type="submit" onClick={() => copyToClipbord()}>COPY</button> : null}
                        </figcaption>
                        <ErrorTextInline text={`${couponAlert}`} style={{ marginTop: '10px' }} />
                    </div>
                </section>


            </div>

        </>
    )

}

export default Invitation;