import React, { useEffect, useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import moment from 'moment';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { selectUser } from '../../../../actions/user/userSlice';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../../constants/global';
import { get, post } from '../../../../util/httpClient';
import { setData, getData } from '../../../../util/localStorageHandler.js';
import withAuthorization from '../../../../Session/withAuthorization';

import Header from '../../../layouts/Header.js';
import Menu from '../../../layouts/Menu.js';

const UserActivity = props => {

    const history = useHistory();

    const [authToken, setAuthToken] = useState("");
    const [activities, setActivities] = useState([]);

    useEffect(() => {
        let data = getData();
        if (data) {
            getActivityDetails(data.api_token);
            setAuthToken(data.api_token);
            // setAuthToken(user.api_token);
        }
    }, [])

    const getActivityDetails = async(authToken) => {

        let path = `${API_BASEURL}/get-user-activity`;
        let res = await get(path, authToken);

        if (res && !res.isError && res.user_activity) {
            res.user_activity && res.user_activity.data && setActivities(res.user_activity.data);
            console.log("user data ===", res.user_activity.data);
        }

    }


    return (
        <>
            <Header title="User Activity" />
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="unlock-membership for-purchase-history">
                        <div class="container">
                            <div class="row">
                                
                                {activities && activities.length>0 ? activities.map((data, index)=>{
                                return<div class="col-lg-6" key={index}>
                                    <div class="settings">
                                        <ul>
                                            <li>
                                                <figure>
                                                    <img src="images/ic13.png" />
                                                </figure>
                                            </li>
                                            <li>
                                                <span>Description: {data.description && data.description} 
                                                <b>{data.action && data.action}</b>
                                                </span>
                                                <span>Credited to: {data.created_at && moment(data.created_at).format('llll')}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                }):
                                    <span>No Activities found...</span>
                                }
                                
                                
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )

}

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(UserActivity);