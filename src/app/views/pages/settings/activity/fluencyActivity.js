import React, { useEffect, useState } from 'react';


import Header from '../../../layouts/Header.js';
import Menu from '../../../layouts/Menu.js';

const FluencyActivity = props => {

    return (
        <>
            <Header title="Fluency Activity"/>
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="introduction">
                        <div class="container">
                            <div class="video-lessons">
                                <h1>Scraped together / In debt</h1>
                                <p>Category name: Idioms</p>
                                <iframe width="100%" height="450" src="https://www.youtube.com/embed/xcJtL7QggTI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>

                            <div class="need-description-one text-center">
                                <div class="descrip-one">--</div>
                                <div class="descrip-two">** This part needs a description **</div>
                            </div>

                            <div class="test-speaking-sec text-center">
                                <strong class="d-block">Now, test your speaking skills by answering the question above.</strong>
                                <p>Rate this Activity</p>
                            </div>

                            <button type="submit" class="btn"><i class="fa fa-microphone"></i>&nbsp; Hold to Answer</button>
                            <button type="submit" class="btn btn-activity">Next Activity</button>
                        </div>
                    </section>
                </div>
            </div>

        </>
    )

}

export default FluencyActivity;