import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';


import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';

const Settings = props => {

    let history = useHistory();

    const goToPage = (path, data) => {
        history.push({
            pathname: path,
            // search: '?query=abc',
            state: data
        });
    }

    return (
        <>
            <Header title="Settings" />
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="unlock-membership">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="unlock-bg">
                                        <div class="figcaption">
                                            <h1>Get Fluent Faster!</h1>
                                            <p class="mb-0">Unlock Your Membership!</p>
                                        </div>

                                        <a href="#">I'm Ready</a>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="settings">
                                        <ul>
                                            <li>
                                                <figure>
                                                    <img src="images/ic12.png" />
                                                </figure>
                                            </li>
                                            <li>
                                                <strong>Billing info</strong>
                                            </li>
                                            <li onClick={() => goToPage('/billingInfo')}>
                                                <a href="javascript:void(0);"><img src="images/right-arrow.png" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="settings">
                                        <ul>
                                            <li>
                                                <figure>
                                                    <img src="images/ic13.png" />
                                                </figure>
                                            </li>
                                            <li>
                                                <strong>User Activity</strong>
                                            </li>
                                            <li onClick={() => goToPage('/activity')}>
                                                <a href="javascript:void(0);"><img src="images/right-arrow.png" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <p class="settings-privacy mt-5 mb-2">
                                <a href="#">Terms & Condition</a> <span class="flt-r"><a href="#">Privacy Policy</a></span>
                            </p>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )

}

export default Settings;