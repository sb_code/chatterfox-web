import React, { useEffect, useState } from 'react';
import moment from 'moment';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';

import { selectUser } from '../../../../actions/user/userSlice';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../../constants/global';
import { get, post } from '../../../../util/httpClient';
import { setData, getData } from '../../../../util/localStorageHandler.js';
import withAuthorization from '../../../../Session/withAuthorization';

import Header from '../../../layouts/Header.js';
import Menu from '../../../layouts/Menu.js';

const BillingInfo = props => {

    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const history = useHistory();

    const [authToken, setAuthToken] = useState("");
    const [email, setEmail] = useState("");
    const [activePlan, setActivePlan] = useState({});
    const [otherPlans, setOtherPlans] = useState([]);


    useEffect(() => {
        setAuthToken(user.api_token);
        getActivePlan(user.api_token);
        getOtherPlans(user.api_token);
    }, [])


    const getActivePlan = async (authToken) => {
        let path = `${API_BASEURL}/active-subscription`;
        let res = await get(path, authToken);
        // console.log("Subscribed plan ==", res);

        if (res && !res.isError) {
            if(res.active_subscription){ 
                res.active_subscription.stripe_status == "active" || res.active_subscription.stripe_status == "trialing" ?
                setActivePlan(res.active_subscription) : setActivePlan({});
            }else{
                setActivePlan({});
            }
        }
    }

    const getOtherPlans = async (authToken) => {

        let path = `${API_BASEURL}/active-subscriptions`;
        let res = await get(path, authToken);

        if (res && !res.isError) {
            res.subscriptions && res.subscriptions.length > 0 ? setOtherPlans(res.subscriptions) : setOtherPlans([]);
        }

    }

    const unsubscribe = async () => {

        let dataToSend = {
            "reason": ''
        };

        let path = `${API_BASEURL}/unsubscribe`;
        let res = await get(path, dataToSend, authToken);

        if (res && !res.isError) {
            Swal.fire('Success!', res.message && res.message, 'success');
            getActivePlan(authToken);
            getOtherPlans(authToken);
        } else {
            Swal.fire('Error!', res.message && res.message, 'error');
        }

    }

    const subscribePlan = async (id) => {

        let dataToSend = {
            "order_id": id,
            "payment_method_nonce": ''
        };

        let path = `${API_BASEURL}/unsubscribe`;
        let res = await post(path, dataToSend, authToken);

        if (res && !res.isError) {
            Swal.fire('Success!', res.message && res.message, 'success');
            getActivePlan(authToken);
            getOtherPlans(authToken);
        } else {
            Swal.fire('Error!', res.message && res.message, 'error');
        }

    }

    const purchaseHistory = (id) => {
        if (id) {
            history.push({
                pathname: "/purchases/"+id,
                state: { "id": id }
            })
        }
    }

    return (
        <>
            <Header title="Billing Info" />
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    {activePlan && activePlan.plan && activePlan.plan.id ?
                        <section class="unlock-membership billing-info">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <strong class="mb-2 active-bill-heading">Active Subscription Plan</strong>
                                        <div class="unlock-bg">
                                            <div class="figcaption">
                                                <h1>{activePlan.plan && activePlan.plan.display_name && activePlan.plan.display_name}</h1>
                                                <p>Status: {activePlan.stripe_status && activePlan.stripe_status}</p>
                                                <p>Start Time: 31st Aug, 2020</p>
                                                <p>Billing amount every month: {activePlan.plan && activePlan.plan.cost && parseFloat(activePlan.plan.cost).toFixed('2') + "$"}</p>
                                                <p>Release Period: {activePlan.plan_duration && activePlan.plan_duration}</p>
                                                <p>Next Billing Time: {activePlan.next_invoice_date && moment(activePlan.next_invoice_date).format('llll')}</p>
                                                <p>Payment Method: {activePlan.payment_method && activePlan.payment_method}</p>
                                            </div>
                                            <div class="info-right">
                                                <a href="JavaScript:Void(0);" onClick={() => purchaseHistory(activePlan.id)}>Purchase History</a>
                                                <span><a href="JavaScript:Void(0);" onClick={() => unsubscribe()}>Unsubscribe this plan</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        : null}
                </div>

                <section class="inactive-subscription">
                    <div class="container">
                        {otherPlans && otherPlans.length > 0 &&
                            <strong class="mb-5 active-bill-heading">Inactive Subscription Plan</strong>}
                        <div class="row">

                            {otherPlans && otherPlans.length > 0 && otherPlans.map((data, index) => {
                                return (data.stripe_status != "active" || data.stripe_status != "trialing" ?
                                    <div class="col-lg-4" key={index}>
                                        <div class="inactive-bg">
                                            <figure>
                                                <img src={data.plan && data.plan.image_url} />
                                            </figure>
                                            <ul>
                                                <li>
                                                    <strong>{data.stripe_status}</strong>
                                                    <p>Status: {data.plan && data.plan.name}</p>
                                                    <p>Start Time: 31st Aug, 2020</p>
                                                    <p>Billing amount every month: {data.plan.cost + "$"}</p>
                                                    <p>Release Period: {data.plan_duration}</p>
                                                    <p>Next Billing Time: {data.next_invoice_date && moment(data.next_invoice_date).format('llll')}</p>
                                                    <p>Payment Method: {data.payment_method}</p>
                                                </li>
                                                <li>
                                                    <a href="JavaScript:Void(0);" class="btn-red" onClick={() => purchaseHistory(data.plan && data.plan.id)}>Purchase History</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> : null)
                            })}

                        </div>
                    </div>
                </section>
            </div>

        </>
    )

}


const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(BillingInfo);