import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import OnboardingLogoText from '../../components/OnboardingLogoText';
import SubmitButtonBig from '../../components/SubmitButtonBig';

const VerificationSuccess = (props) => {
  let history = useHistory();

  const [email, setEmail] = useState("");
  const [coupon, setCoupon] = useState("");
  const [password, setPassword] = useState("");


  useEffect(() => {
    if (history && history.location && history.location.state && history.location.state.email && history.location.state.coupon && history.location.state.password) {
      let email = history.location.state.email;
      let coupon = history.location.state.coupon;
      let password = history.location.state.password;
      setEmail(email);
      setCoupon(coupon);
      setPassword(password);
    } else {
      history.push('/signin');
    }
  }, [])


  const goToPage = (path) => {
    history.push(path);
  }


  return (
    <section class="login sec-pad">
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
      </Helmet>
      <div class="container">
        {/* <div class="left-arrow">
          <a href="#"><i class="fa fa-arrow-left"></i></a>
        </div> */}
        {/* <div class="figure d-block text-center">
          <img src="images/logo.png" />
        </div>
        <div class="figcaption text-center">
          <div class="site-navbar d-block mb-2">
            <h2>Success</h2>
            <p>You have successfully registered your account.</p>
          </div>
        </div> */}
        <OnboardingLogoText text="Success" subText="You have successfully registered your account." />
        <div class="login-video extra-padding">
          <strong class="d-block mb-2">
            {email && "Your email is: " + email} <br />
            {password && "Your password is: " + password} <br />
            {coupon && "Your coupon is: " + coupon}
          </strong>

          {/* <iframe width="100%" height="480" src="https://www.youtube.com/embed/xcJtL7QggTI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> */}
          <iframe src="https://player.vimeo.com/video/486387881" width="100%" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

          {/* <button type="submit" class="btn-red btn-orange">Go to Home page</button> */}
          <SubmitButtonBig caption="Go to Home page" onClick={() => goToPage('/modules')} />
        </div>
      </div>
    </section>
  );
};

export default VerificationSuccess;