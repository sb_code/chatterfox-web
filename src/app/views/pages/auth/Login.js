import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Helmet } from "react-helmet";
import { GoogleLogin } from 'react-google-login';
// import FacebookLogin from 'react-facebook-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import ReCAPTCHA from "react-google-recaptcha";
import { Modal, Button } from 'react-bootstrap';
import ActionLink from '../../components/ActionLink';
import OnboardingLogoText from '../../components/OnboardingLogoText';
import SubmitButtonBig from '../../components/SubmitButtonBig';
import { isUserLoggedin, loginWithCaptcha } from '../../../services/authServices';
import Swal from 'sweetalert2';
import ErrorTextInline from '../../components/ErrorTextInline';
import { API_BASEURL, CF_USER, RE_CAPTCHA_WEB_KEY } from '../../../constants/global';
import { post } from '../../../util/httpClient';
import { setUser } from '../../../actions/user/userSlice';
import { useDispatch } from 'react-redux';
import { decryptData, encryptData } from '../../../util/cryptoConverter';
import { getData } from '../../../util/localStorageHandler';
// import PropTypes from 'prop-types';

const Login = props => {
  let history = useHistory();
  let dispatch = useDispatch();

  let [email, setEmail] = useState("");
  let [modalMail, setModalMail] = useState("");
  let [show, setShow] = useState(false);
  let [loginStatus, setLoginStatus] = useState("");
  let [payload, setPayload] = useState({});
  let [password, setPassword] = useState("");
  let [passwordType, setPasswordType] = useState("password");
  let [isLoading, setLoading] = useState(false);
  // let [error, setError] = useState("");
  let [emailError, setEmailError] = useState("");
  let [passwordError, setPasswordError] = useState("");
  let [show_captcha, setShowCaptcha] = useState(false);
  let [reCaptchaToken, setCaptchaToken] = useState("");

  useEffect(() => {
    isUserLoggedin(history);
  }, []);

  const goToPage = (path, data) => {
    history.push({
      pathname: path,
      // search: '?query=abc',
      state: data
    });
  }

  const login = async () => {
    // console.log(encryptData({name:'subh',is:true}));
    // console.log(decryptData('U2FsdGVkX1+p2wV0R9Ck1tjFF/omE8U69hLfpxLE8biI8kSG/9/SuZdWgC4FbTrA'));
    let isValid = true
    // Start loading
    setLoading(true);
    // Validate field
    if (!email) { isValid = false; setEmailError("Enter your email id."); }
    if (!password) { isValid = false; setPasswordError("Enter your password."); }

    // If validate
    if (isValid) {
      try {
        // Login user
        let userData = await loginWithCaptcha(email, password, reCaptchaToken);
        dispatch(setUser(userData));
        setLoading(false);
        goToPage('/modules');
      } catch (error) {
        if (error.show_captcha) {
          setShowCaptcha(true);
        }
        Swal.fire("Error!", error.message ? error.message : 'Some error occurred. Please try again.', "error!");
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  }

  const responseGoogle = async (response) => {
    console.log('responseGoogle == ', response);
    if (!response || response.error) {
      Swal.fire('Error!', response.details ? response.details : response.error, 'error')
    } else {
      // console.log("All google data for sign in ==", response);
      let socialId = response.googleId;
      let email = response.profileObj && response.profileObj.email ? response.profileObj.email : '';
      let accessToken = response.accessToken;
      let dataToSend = {
        "token": accessToken,
        "role": CF_USER,
        "email": email
      }
      if (email && email != '') {
        googleSignIn(dataToSend);
      } else {
        setLoginStatus('gmail');
        setShow(true);
        setPayload(dataToSend);
      }
    }
  }

  const googleSignIn = async (dataToSend) => {
    setLoading(true);
    let url = `${API_BASEURL}/google-login`;
    let response = await post(url, dataToSend);
    if (response && !response.isError) {
      dispatch(setUser(response.data && response.data));
      Swal.fire("Success", response.message && response.message, "success");
      setLoading(false);
      goToPage('/modules');
    } else {
      Swal.fire("Error!", 'Some error occurred. Please try again.', "error");
      setLoading(false);
    }
    setLoginStatus("");
    setModalMail("");
    setPayload({});
  }

  const responseFacebook = async (response) => {

    if (!response || response.error) {
      Swal.fire('Error!', response.error, 'error')
    } else if (response.status && response.status === 'unknown') {
      console.log("User closed popup");
    } else {
      console.log("Response from facebook sign in ==", response);
      let token = response.accessToken;
      let email = response.email ? response.email : '';
      let name = response.name;

      let dataToSend = {
        "token": token,
        "role": CF_USER,
        "email": email
      }
      if (email && email != '') {
        facebookSignIn(dataToSend);
      } else {
        setLoginStatus('fb');
        setPayload(dataToSend);
        setShow(true);
      }
    }

  }

  const facebookSignIn = async (dataToSend) => {

    setLoading(true);
    let url = `${API_BASEURL}/facebook-login`;
    let response = await post(url, dataToSend);
    if (response && !response.isError) {
      dispatch(setUser(response.data && response.data));
      Swal.fire("Success", response.message && response.message, "success");
      setLoading(false);
      goToPage('/modules');
    } else {
      Swal.fire("Error!", 'Some error occurred. Please try again.', "error");
      setLoading(false);
    }
    setLoginStatus("");
    setModalMail("");
    setPayload({});

  }

  const gotoForgotPassword = async () => {
    goToPage('/forgot-password');
  }

  const toggleShowPassword = () => {
    let e = document.getElementById('password-field');
    if (e.type == 'password') {
      setPasswordType('text');
    } else {
      setPasswordType('password');
    }
  }

  const handleClose = () => {
    setShow(false);
  }

  const handleSubmit = () => {

    if (loginStatus == 'gmail') {
      payload.email = modalMail;
      googleSignIn(payload);
    } else if (loginStatus == 'fb') {
      payload.email = modalMail;
      facebookSignIn(payload);
    } else {
      console.log("No login status ia available -- ");
    }
    setShow(false);

  }

  const onChangeReCAPTCHA = (value) => {
    console.log('onChangeReCAPTCHA', value);
    setCaptchaToken(value);
  }

  return (
    <>
      <section class="login sec-pad">
        <Helmet>
          <meta name="keywords" content="Chatterfox" />
        </Helmet>
        <div class="container">
          {/* <div class="figure d-block text-center">
          <img src="images/logo.png" />
        </div>
        <div class="figcaption text-center">
          <div class="site-navbar d-block">
            <h2>Login</h2>
          </div>
        </div> */}
          <OnboardingLogoText text="Login" />
          <div class="figcaption text-center">
            <span class="social-sec-login">
              {/* <a href="JavaScript:Void(0);"><img src="images/sc1.jpg" /></a> */}

              <FacebookLogin
                appId="562262044334105"
                autoLoad={false}
                fields="name,email"
                callback={responseFacebook}
                render={renderProps => (
                  // <button onClick={renderProps.onClick}>This is my custom FB button</button>
                  <a href="JavaScript:Void(0);" onClick={renderProps.onClick} disabled={renderProps.disabled}>
                    <img src="images/sc2.jpg" />
                  </a>
                )}
              />
              <GoogleLogin
                // clientId="76245937142-o25p14ae63g8ctg56i54ah39sqspg38l.apps.googleusercontent.com"
                clientId="1076767246869-5c8ecj7o5nsdj434km1l2ct3qo9bpqg5.apps.googleusercontent.com"
                render={renderProps => (
                  // <button type="button" class="btn signin google" onClick={renderProps.onClick} disabled={renderProps.disabled}>Sign In with Google</button>
                  <a href="JavaScript:Void(0);" onClick={renderProps.onClick} disabled={renderProps.disabled} >
                    <img src="images/sc3.jpg" />
                  </a>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
              />
            </span>
          </div>
          <div class="login-form extra-padding">
            <form>
              <p class="text-center mb-2">Login with</p>
              <input
                type="email"
                class="form-control"
                placeholder="Email address"
                value={email}
                onChange={e => { setEmail(e.target.value); setEmailError(""); }}
              />
              <div>
                <input
                  type={passwordType}
                  id="password-field"
                  class="form-control"
                  placeholder="Password"
                  value={password}
                  onChange={e => {
                    setPassword(e.target.value);
                    setPasswordError("");
                  }}
                />
                <span
                  class={passwordType == 'text' ? "fa fa-fw fa-eye field-icon toggle-password" : "fa fa-fw fa-eye-slash field-icon toggle-password"}
                  onClick={() => toggleShowPassword()}>
                </span>
              </div>
              <p class="text-right">
                <ActionLink caption="Forgot Password?" onClick={() => gotoForgotPassword()} />
              </p>
              {show_captcha && <ReCAPTCHA
                sitekey={RE_CAPTCHA_WEB_KEY}
                onChange={onChangeReCAPTCHA}
              />}
              <SubmitButtonBig caption="Login" isLoading={isLoading} onClick={() => login()} />
              {/* <button type="submit" class="btn-red btn-orange">Login</button> */}
              <p class="text-center">
                Don't have an account? <ActionLink caption="Become a ChatterFox" onClick={() => goToPage('/signup')} />
              </p>
            </form>
            <ErrorTextInline text={` ${emailError} ${passwordError}`} />
          </div>
        </div>
      </section>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Give your Email Id</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input
            type="email"
            class="form-control"
            placeholder="email id"
            value={modalMail}
            onChange={e => setModalMail(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>Close</Button>
          <Button variant="primary" onClick={handleSubmit}>Submit</Button>
        </Modal.Footer>
      </Modal>

    </>
  );
};

// Login.propTypes = {

// };

export default Login;