import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import { API_BASEURL } from '../../../constants/global';
import { sendPasswordResetOTP } from '../../../services/authServices';
import { post } from '../../../util/httpClient';
import ActionLink from '../../components/ActionLink';
import BackButton from '../../components/BackButton';
import ErrorTextInline from '../../components/ErrorTextInline';
import OnboardingLogoText from '../../components/OnboardingLogoText';
import SubmitButtonBig from '../../components/SubmitButtonBig';
// import PropTypes from 'prop-types';

const ForgotPassword = props => {
  let history = useHistory();
  let [email, setEmail] = useState("");
  let [isLoading, setLoading] = useState(false);
  let [emailError, setEmailError] = useState("");

  // useEffect(() => {
  //   let email = history && history.location && history.location.state && history.location.state.email ? history.location.state.email : '';
  //   if (!email) {
  //     goToPage('/signin');
  //   } else {
  //     setEmail(email);
  //   }
  // }, []);

  const goToPage = (path, data) => {
    history.push({
      pathname: path,
      // search: '?query=abc',
      state: data
    });
  }


  const sendForgotPasswordOTP = async () => {
    let isValid = true
    setLoading(true);
    // Validate field
    if (!email) { isValid = false; setEmailError("Enter your email id."); }
    // If validate
    if (isValid) {
      try {
        let result = await sendPasswordResetOTP(email)
        setLoading(false);
        goToPage('/reset-password', { email });
      } catch (error) {
        setLoading(false);
        Swal.fire("Error!", error.message ? error.message : 'Some error occurred. Please try again.', 'error');
        // setError(error.message ? error.message : 'Some error occurred. Please try again.')
      }
    } else {
      // goToPage('/email-verification', {coupon});
      setLoading(false);
    }
  }



  return (
    <section class="login sec-pad">
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
      </Helmet>
      <div class="container">
        <BackButton onClick={() => goToPage('/signin')} />
        <OnboardingLogoText text="Forgot Password!" />
        {/* <div class="figure d-block text-center">
          <img src="images/logo.png" />
        </div>
        <div class="figcaption text-center">
          <div class="site-navbar d-block mb-2">
            <h2>Register</h2>
          </div>
        </div> */}
        <div class="login-form extra-padding">
          <form>
            <p class="text-center mb-2">Please enter your registered email below:</p>
            <input
              type="email"
              class="form-control"
              placeholder="Email address"
              value={email}
              onChange={e => { setEmail(e.target.value); setEmailError(""); }}
            />
            <SubmitButtonBig caption="Send OTP" onClick={() => sendForgotPasswordOTP()} isLoading={isLoading} />
            {/* <button type="submit" class="btn-red btn-orange">Verify</button> */}
            {/* <p class="text-center">
            Didn't get the verification code? <ActionLink caption="Resend" onClick={() => resendOtp()} />
          </p> */}
          </form>
          <ErrorTextInline text={`${emailError}`} />
          {/* <p class="text-center warning">You have entered a wrong code.</p> */}
        </div>
      </div>
    </section>
  );
}
// ForgotPassword.propTypes = {

// };

export default ForgotPassword;