import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { API_BASEURL, CF_USER } from '../../../constants/global';
import BackButton from '../../components/BackButton';
import OnboardingLogoText from '../../components/OnboardingLogoText';
import SubmitButtonBig from '../../components/SubmitButtonBig';
import { post } from '../../../util/httpClient.js';
import Swal from 'sweetalert2';
import ErrorTextInline from '../../components/ErrorTextInline';
import { applyCoupon, internalLogin, checkCouponValidity, isUserLoggedin } from '../../../services/authServices';
import { useDispatch } from 'react-redux';
import { setUser, setUserAsync } from '../../../actions/user/userSlice';
import { Helmet } from 'react-helmet';

// import PropTypes from 'prop-types';

const Signup = props => {
  let history = useHistory();
  const dispatch = useDispatch();

  let [email, setEmail] = useState("");
  let [role_id, setRoleid] = useState(CF_USER);
  let [password, setPassword] = useState("");
  let [password_confirmation, setPasswordConfirmation] = useState("");
  let [coupon, setCoupon] = useState("");
  let [passwordType, setPasswordType] = useState("password");

  let [isLoading, setLoading] = useState(false);

  let [emailError, setEmailError] = useState("");
  let [role_idError, setRoleidError] = useState("");
  let [passwordError, setPasswordError] = useState("");
  let [password_confirmationError, setPasswordConfirmationError] = useState("");
  let [couponError, setCouponError] = useState("");
  let [error, setError] = useState("");

  const goToPage = (path, data) => {
    history.push({
      pathname: path,
      // search: '?query=abc',
      state: data
    });
  }

  useEffect(() => {
    isUserLoggedin(history);
  }, []);

  const checkReferralCode = async () => {
    if (coupon) {
      try {
        let response = await checkCouponValidity(coupon);
        console.log(response);
        if (response) {
          Swal.fire('Info', 'Referral code is valid', 'info');
        } else {
          Swal.fire('Info', 'Referral code is not valid', 'info');
        }
      } catch (error) {
        Swal.fire("Error!", error.message ? error.message : 'Some error occurred. Please try again.', "error");
      }
    } else {
      Swal.fire('Error', 'Enter code.', 'error');
    }

  }

  /**
   * email.not@gmail.com registered non exsist email
   */
  const registerUser = async () => {
    let isValid = true
    // Start loading
    setLoading(true);
    // Validate field
    if (!email) { isValid = false; setEmailError("Enter your email id."); }
    if (!password) { isValid = false; setPasswordError("Enter your password."); }
    if (coupon) {
      try {
        let response = await checkCouponValidity(coupon);
        if (response) { } else {
          isValid = false; setCouponError('Referral code is not valid');
        }
      } catch (error) {
        isValid = false; setCouponError('Referral code is not valid');
      }
    }
    // If validate
    if (isValid) {
      let url = `${API_BASEURL}/register`;
      let dataToSend = { email, role_id, password, password_confirmation };
      try {
        // Register user
        let response = await post(url, dataToSend);
        console.log(response);
        if (response && !response.isError) {
          // // Need internal login to apply coupon code.
          // let userData = await internalLogin(email, password);
          // dispatch(setUser(userData));
          // // Apply Coupon if any 
          // if (coupon) {
          //   try {
          //     let authToken = userData.api_token;
          //     let serverRes = await applyCoupon(coupon,authToken);
          //   } catch (error) {
          //     console.log('can not apply coupon');
          //   }
          // }
          setLoading(false);
          goToPage('/email-verification', { coupon, email, password });
        } else {
          // errorCode: 400, message: "The email has already been taken."
          Swal.fire(response.message);
          setLoading(false);
        }
      } catch (error) {
        Swal.fire("Error!", error.message ? error.message : 'Some error occurred. Please try again.', "error");
        setLoading(false);
      }
    } else {
      // goToPage('/email-verification', {coupon});
      setLoading(false);
    }

  }

  const toggleShowPassword = () => {
    let e = document.getElementById('password-field');
    if (e.type == 'password') {
      setPasswordType('text');
    } else {
      setPasswordType('password');
    }
  }

  return (
    <section class="login sec-pad">
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
      </Helmet>
      <div class="container">
        <BackButton onClick={() => goToPage('/signin')} />
        <OnboardingLogoText text="Register" />

        {/* <div class="figure d-block text-center">
          <img src="images/logo.png" />
        </div>
        <div class="figcaption text-center">
          <div class="site-navbar d-block mb-2">
            <h2>Register</h2>
          </div>
        </div> */}
        <div class="login-form extra-padding">
          <form>
            <input
              type="email"
              class="form-control"
              placeholder="Email address"
              value={email}
              onChange={e => { setEmail(e.target.value); setEmailError(""); }}
            />
            <div>
              <input
                type={passwordType}
                id="password-field"
                class="form-control"
                placeholder="Password"
                value={password}
                onChange={e => {
                  setPassword(e.target.value);
                  setPasswordConfirmation(e.target.value);
                  setPasswordError("");
                  setPasswordConfirmationError("");
                }}
              />
              <span
                class={passwordType == 'text' ? "fa fa-fw fa-eye field-icon toggle-password" : "fa fa-fw fa-eye-slash field-icon toggle-password"}
                onClick={() => toggleShowPassword()}>
              </span>
            </div>
            <span class="d-flex align-items-center reffer-code">
              <input
                type="text"
                class="form-control"
                placeholder="Referral code if Available"
                value={coupon}
                onChange={e => { setCoupon(e.target.value); setCouponError(""); }}
              />
              <a href="javascript:void(0)" onClick={() => checkReferralCode()}>CHECK VALIDITY</a>
            </span>
            <SubmitButtonBig caption="Become a ChatterFox" onClick={() => registerUser()} isLoading={isLoading} />
            {/* <button type="submit" class="btn-red btn-orange">Become a ChatterFox</button> */}
          </form>
          <ErrorTextInline text={` ${emailError} ${passwordError} ${couponError} ${error}`} />
        </div>
      </div>
    </section>
  );
};

// Signup.propTypes = {

// };

export default Signup;