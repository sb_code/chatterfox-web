import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import { API_BASEURL } from '../../../constants/global';
import { sendPasswordResetOTP } from '../../../services/authServices';
import { post } from '../../../util/httpClient';
import ActionLink from '../../components/ActionLink';
import BackButton from '../../components/BackButton';
import ErrorTextInline from '../../components/ErrorTextInline';
import OnboardingLogoText from '../../components/OnboardingLogoText';
import SubmitButtonBig from '../../components/SubmitButtonBig';
// import PropTypes from 'prop-types';

const ResetPassword = props => {
  let history = useHistory();
  let [otp, setOTP] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [repassword, setRepassword] = useState("");
  let [passwordType, setPasswordType] = useState("password");
  let [repasswordType, setRepasswordType] = useState("password");
  let [isLoading, setLoading] = useState(false);
  let [otpError, setOTPError] = useState("");
  let [passwordError, setPasswordError] = useState("");
  let [repasswordError, setRepasswordError] = useState("");

  useEffect(() => {
    let email = history && history.location && history.location.state && history.location.state.email ? history.location.state.email : '';
    if (!email) {
      goToPage('/forgot-password');
    } else {
      setEmail(email);
    }
  }, []);

  const goToPage = (path) => {
    history.push(path);
  }


  const toggleShowPassword = () => {
    let e = document.getElementById('password-field');
    if (e.type == 'password') {
      setPasswordType('text');
    } else {
      setPasswordType('password');
    }
  }

  const toggleShowRepassword = () => {
    let e = document.getElementById('repassword-field');
    if (e.type == 'password') {
      setRepasswordType('text');
    } else {
      setRepasswordType('password');
    }
  }

  const resendOtp = async () => {
    try {
      let result = await sendPasswordResetOTP(email)
      Swal.fire('Success', result.message ? result.message : 'OTP has been sent.', 'success');
    } catch (error) {
      Swal.fire('Error', error.message ? error.message : 'Some error occurred. Please try again.', 'error');
    }
  }

  const resetPassword = async () => {
    let isValid = true
    // Start loading
    setLoading(true);
    // email
    // otp
    // newPassword
    if (!email) { isValid = false; }
    if (!otp) { isValid = false; setOTPError("Enter your OTP."); }
    if (!password) { isValid = false; setPasswordError("Enter New Password"); }
    if (!repassword) { isValid = false; setRepasswordError("Reenter your Password"); }
    if (repassword && password != repassword) { isValid = false; setRepasswordError("New Password and Reenter Password should match."); }

    // If validate
    if (isValid) {
      let url = `${API_BASEURL}/reset-password`;
      let dataToSend = { email, otp, newPassword: password };
      try {
        // Verify email
        let response = await post(url, dataToSend);
        console.log(response);
        if (response && !response.isError) {
          setLoading(false);
          Swal.fire({
            // position: 'top-end',
            icon: 'success',
            title: 'Your password has been reset.',
            showConfirmButton: false,
            timer: 1500
          })
          goToPage('/signin');
        } else {
          Swal.fire('Error', response && response.message ? response.message : 'Invalid OTP', 'error');
          setLoading(false);
        }
      } catch (error) {
        Swal.fire('Error', error.message ? error.message : 'Some error occurred. Please try again.', 'error');
        setLoading(false);
      }
    } else {
      setLoading(false);
    }

    // goToPage('/signin');
  }
  return (
    <section class="login sec-pad">
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
      </Helmet>
      <div class="container">
        <BackButton onClick={() => goToPage('/signin')} />
        <OnboardingLogoText text="Forgot Password!" />
        {/* <div class="figure d-block text-center">
            <img src="images/logo.png" />
          </div>
          <div class="figcaption text-center">
            <div class="site-navbar d-block mb-2">
              <h2>Register</h2>
            </div>
          </div> */}
        <div class="login-form extra-padding">
          <form>
            <p class="text-center mb-2">Please check your email and enter the one time verification code below:</p>

            <input
              type="text"
              class="form-control"
              placeholder="OTP"
              value={otp}
              onChange={e => {
                setOTP(e.target.value);
                setOTPError("");
              }}
            />

            <div>
              <input
                type={passwordType}
                id="password-field"
                placeholder="New Password"
                class="form-control"
                value={password}
                onChange={e => {
                  setPassword(e.target.value);
                  setPasswordError("");
                }}
              />
              <span
                class={passwordType == 'text' ? "fa fa-fw fa-eye field-icon toggle-password" : "fa fa-fw fa-eye-slash field-icon toggle-password"}
                onClick={() => toggleShowPassword()}>
              </span>
            </div>
            <div>
              <input
                type={repasswordType}
                id="repassword-field"
                placeholder="Reenter new password"
                class="form-control"
                value={repassword}
                onChange={e => {
                  setRepassword(e.target.value);
                  setRepasswordError("");
                }}
              />
              <span
                class={repasswordType == 'text' ? "fa fa-fw fa-eye field-icon toggle-password" : "fa fa-fw fa-eye-slash field-icon toggle-password"}
                onClick={() => toggleShowRepassword()}>
              </span>
            </div>
            <SubmitButtonBig caption="Reset Password" onClick={() => resetPassword()} isLoading={isLoading} />
            {/* <button type="submit" class="btn-red btn-orange">Verify</button> */}
            <p class="text-center">
              Didn't get the verification code? <ActionLink caption="Resend" onClick={() => resendOtp()} />
            </p>
          </form>
          <ErrorTextInline text={`${otpError} ${passwordError} ${repasswordError}`} />
          {/* <p class="text-center warning">You have entered a wrong code.</p> */}
        </div>
      </div>
    </section>
  );
};

// ForgotPassword.propTypes = {

// };

export default ResetPassword;