import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import { setUser } from '../../../actions/user/userSlice';
import { API_BASEURL } from '../../../constants/global';
import { applyCoupon, internalLogin, sendOTP } from '../../../services/authServices';
import { post } from '../../../util/httpClient';
import ActionLink from '../../components/ActionLink';
import BackButton from '../../components/BackButton';
import ErrorTextInline from '../../components/ErrorTextInline';
import OnboardingLogoText from '../../components/OnboardingLogoText';
import SubmitButtonBig from '../../components/SubmitButtonBig';
// import PropTypes from 'prop-types';

const EmailVerification = props => {
  let history = useHistory();
  const dispatch = useDispatch();

  console.log(history.location.state.coupon);
  let [coupon, setCoupon] = useState('')
  let [otp, setOtp] = useState('')
  let [email, setEmail] = useState('')
  let [password, setPassword] = useState('')
  let [isLoading, setLoading] = useState(false);

  let [otpError, setOtpError] = useState('')
  let [emailError, setEmailError] = useState("");

  useEffect(() => {
    let coupon = history && history.location && history.location.state && history.location.state.coupon ? history.location.state.coupon : '';
    let email = history && history.location && history.location.state && history.location.state.email ? history.location.state.email : '';
    let password = history && history.location && history.location.state && history.location.state.password ? history.location.state.password : '';

    if (!email || !password) {
      goToPage('/signup');
    } else {
      setCoupon(coupon);
      setEmail(email);
      setPassword(password);
    }

  }, []);

  const goToPage = (path) => {
    history.push(path);
  }

  const resendOtp = async () => {
    try {
      let result = await sendOTP(email)
      Swal.fire('Success', result.message ? result.message : 'OTP has been sent.', 'success');
    } catch (error) {
      Swal.fire('Error', error.message ? error.message : 'Some error occurred. Please try again.', 'error');
    }
  }

  const verifyEmail = async () => {
    let isValid = true
    // Start loading
    setLoading(true);
    if (!otp) { isValid = false; setOtpError("Enter your verification code."); }
    if (!email) { isValid = false; setEmailError("No email found"); }

    // If validate
    if (isValid) {
      let url = `${API_BASEURL}/verify-email`;
      let dataToSend = { email, otp };
      try {
        // Verify email
        let response = await post(url, dataToSend);
        console.log(response);
        if (response && !response.isError) {
          // // Need internal login to apply coupon code.
          let userData = await internalLogin(email, password);
          dispatch(setUser(userData));
          // Apply Coupon if any 
          if (coupon) {
            try {
              let authToken = userData.api_token;
              let serverRes = await applyCoupon(coupon, authToken);
            } catch (error) {
              console.log('can not apply coupon');
            }
          }
          setLoading(false);
          goToPage('/verification-success');
        } else {
          Swal.fire(response.message);
          setLoading(false);
        }
      } catch (error) {
        Swal.fire('Error', error.message ? error.message : 'Some error occurred. Please try again.', 'error');
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  }

  return (
    <section class="login sec-pad">
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="robots" content="noindex,nofollow" />
      </Helmet>
      <div class="container">
        <BackButton onClick={() => goToPage('/signup')} />
        <OnboardingLogoText text="Register" />
        {/* <div class="figure d-block text-center">
          <img src="images/logo.png" />
        </div>
        <div class="figcaption text-center">
          <div class="site-navbar d-block mb-2">
            <h2>Register</h2>
          </div>
        </div> */}
        <div class="login-form extra-padding">
          <form>
            <p class="text-center mb-2">Please check your email and enter the one time verification code below:</p>
            <input
              type="text"
              class="form-control"
              placeholder="Verification code"
              value={otp}
              onChange={e => { setOtp(e.target.value); setOtpError(""); }}
            />
            <SubmitButtonBig caption="Verify" onClick={() => verifyEmail()} isLoading={isLoading} />
            {/* <button type="submit" class="btn-red btn-orange">Verify</button> */}
            <p class="text-center">
              Didn't get the verification code? <ActionLink caption="Resend" onClick={() => resendOtp()} />
            </p>
          </form>
          <ErrorTextInline text={`${otpError}`} />
          {/* <p class="text-center warning">You have entered a wrong code.</p> */}
        </div>
      </div>
    </section>
  );
};

// EmailVerification.propTypes = {

// };

export default EmailVerification;