import React, { useEffect, useState } from 'react';

// import Header from '../../layouts/Header.js';
// import Menu from '../../layouts/Menu.js';

const FinalLesson = props => {

    const [lesson, setLesson] = useState({});

    useEffect(()=>{
        props.lessonData && props.lessonData.length>0 && setLesson(props.lessonData[0]);
        console.log("props data ==", props.lessonData);
    },[])

    const postLesson= ()=> {

    }


    return (
        <>
            {/* <Header title="Lesson"/> */}
            {/* <Menu /> */}
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="introduction">
                        <div class="container">
                            <div class="figure"> 
                                {lesson.image_first_url? <img src={lesson.image_first_url} />: <img src="images/intro-img.jpg" />}
                            </div>
                            <div class="figcaption text-center">
                                <p>Some people think student debt is unfair and that the government should use tax revenue to pay for students' tuition. Other people think that students should be responsible for their own tuition bills.</p>
                                <p>What do you think?</p>
                                <p>I (think/don't think) university students should go into debt to pay for tuition because</p>
                                <p>____________________________</p>
                                <p>I think the government (should/shouldn't) use tax revenue to pay for tuition</p>
                                <p>because____________________________</p>
                                <p>In my country_______________________</p>
                                <p>Try to use as many of these words as you can!</p>
                                <p>Debt tax dollars government tuition student get amount but want limit can't better quality ability a lot.</p>
                            </div>
                            <button type="submit" class="btn"><i class="fa fa-microphone"></i>&nbsp; Hold to Record</button>
                            <button type="submit" class="btn btn-activity" onClick={()=>postLesson()}>Next post Lesson</button>
                        </div>
                    </section>
                </div>
            </div>

        </>
    )

}

export default FinalLesson;