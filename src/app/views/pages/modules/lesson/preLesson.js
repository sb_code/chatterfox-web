import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../../actions/breadcrumps/breadCrumps';
import AudioPlayer from '../../../components/AudioPlayer';
import ReactPlayer from 'react-player'

import { ReactMic } from 'react-mic';

import { submitUserRecordedData } from '../../../../services/lessonsServices';
import { setData, getData } from '../../../../util/localStorageHandler.js';
import { confirmAlert } from 'react-confirm-alert';
import '../../../../../../node_modules/react-confirm-alert/src/react-confirm-alert.css';

const PreLesson = props => {

    let breadcrumps = useSelector(selectBreadCrumpsPath);
    let history = useHistory();
    let dispatch = useDispatch();
    let lesson = props.lessonData;
    let userData = ""


    const [state, setState] = useState({
      record: false,
      recordData:"",
      assignment_file:lesson.assignment_file
    });

    useEffect(() => { 
        //dispatch(setBreadCrumpsPath( {"paths":lesson.lessonData.title+"_2","index":2,"route":"modules", "payload":{}} ));
        //dispatch(setBreadCrumpsPath( {"paths":"pre-Lesson_2","index":2,"route":"preLesson", "payload":{}} ));

    }, []);

    const onData = (recordedBlob) => {
        console.log('chunk of real-time data is: ', recordedBlob);
    }
     
    const onStop = (recordedBlob) => {
        console.log('recordedBlob is: ', recordedBlob);
        setState({ recordData:recordedBlob })
    }

    const blobToFile = (theBlob, fileName) =>{
        //A Blob() is almost a File() - it's just missing the two properties below which we will add
        // theBlob.lastModifiedDate = new Date();
        // theBlob.name = fileName;
        // return theBlob;
        console.log("theBlob...",theBlob)
        var blob = URL.createObjectURL(theBlob.blob);
          console.log(blob);

          var fileType = 'audio'; // or "audio"
          var fileName = theBlob.blob.type + '.mp3';  // or "wav"

          var formData = new FormData();
          var boundary = String(Math.random()).slice(2);

          //formData.append("event_id", window.event_id);
          formData.append("type", theBlob.blob.type);

          formData.append(fileType + '-filename', fileName);
          formData.append(fileType + '-blob', blob);

          return formData;
    }

    const submitRecord = (ev) =>{
      // ev.preventDefault();
      // let file = ev.target.files[0];
      
        userData = getData();
       
        var h = new File([state.recordData], "new.mp3", {type: "audio/mpeg", lastModified: new Date()});

        const formData = new FormData();
        
        formData.append("recorded_file", h )
        formData.append("assessment_id",231)
        formData.append("assessment_type","pre_lesson")
        formData.append("duration","02.05")
        //console.log("state.recordData...",state.recordData);

        submitUserRecordedData( formData ,userData.api_token)
    } 

    const deleteRecord = () => {
        confirmAlert({
          title: 'Confirm to cancel',
          message: 'Are you sure to do this.',
          buttons: [
            {
              label: 'Yes',
              onClick: () => setState({ recordData:"" })
            },
            {
              label: 'No',
              onClick: () => console.log("hello")
            }
          ]
        })
    }

    const recordAudio = () =>{
        if(state.recordData){
            confirmAlert({
              title: 'Audio file detected',
              message: 'Please remove existing audio file first',
              buttons: [
                
                {
                  label: 'Ok',
                  onClick: () => console.log("hello")
                }
              ]
            })
        }else{
            setState({ record:true })
        }
        
    }

    console.log("lesson....",lesson);


    return (
        <>
           
            <section className="introduction">
                <div className="container">
                    {lesson.file_type == "image"?
                        <div className="figure">
                            <img src={lesson.file_url?lesson.file_url:process.env.PUBLIC_URL + '/images/intro-img.png'} />
                        </div>
                        :
                        <ReactPlayer url={lesson.file_url?lesson.file_url:"https://www.youtube.com/watch?v=ysz5S6PUM-U"} />
                    }
                    <div className="figcaption text-center pb-3">
                        <p>{lesson.description?lesson.description:"no data"}</p>
                       
                    </div>
                    {state.assignment_file?

                        <div className="row">
                            <AudioPlayer source = {state.assignment_file} duration ={122} />
                            
                            <button type="submit" className="btn col-sm-3" 
                                onClick={()=>{props.next(lesson)}} 
                            >   
                                Next
                            </button>

                           
                        </div>
                        
                    :
                        <div className="col-sm-12 p-0 text-center">
                            <ReactMic
                                  record={state.record}
                                  className="sound-wave"
                                  //mimeType="audio/mp3"
                                  onStop={onStop}
                                  onData={onData}
                                  strokeColor="transparent"
                                  backgroundColor="transparent"
                            />
                          
                            {!state.record?
                                <div className="intro-video-sec text-center">
                                    <a className="theme-circle" href="JavaScript:void(0);"  
                                        onClick={() => recordAudio() } 
                                    >
                                        <img src={process.env.PUBLIC_URL + '/images/microphone.png'} />
                                    </a>
                                    <span className="d-block mt-2">Tap to record</span>
                                </div>
                            :
                                <div className="intro-video-sec text-center">
                                    <a href="JavaScript:void(0);" className="theme-circle"  
                                        onClick={() => setState({ record:false }) }
                                    >
                                        <img src={process.env.PUBLIC_URL + '/images/stop.png'} />
                                    </a>
                                    <span className="d-block mt-2">Tap to stop recording</span>
                                </div>
                            }

                            {state.recordData?
                                <div className="col-sm-12 p-0 text-center float-left">
                                    <AudioPlayer source = {state.recordData.blobURL} duration ={122} />
                                      <input type="file" id="fileType" onChange = {(event) => setState({ recordData:event.target.files[0] }) } />
                                                   
                                    <button type="submit" className="btn col-sm-3 d-inline mr-3" 
                                        onClick={() => deleteRecord() } 
                                    >   
                                        Cancel
                                    </button>
                                    <button type="submit" className="btn col-sm-3 d-inline" 
                                        onClick={() => submitRecord() } 
                                    >   
                                        Submit
                                    </button>
                                </div>
                            :null}

                             <button type="submit" className="btn col-sm-3" 
                                onClick={()=>{props.next(lesson)}} 
                            >   
                                Next
                            </button>
                        </div>
                    }
                   
                </div>
            </section>
             

        </>
    )

}

export default PreLesson;