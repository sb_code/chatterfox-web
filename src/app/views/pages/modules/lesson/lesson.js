import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectLessonStore, setLessonStore } from '../../../../actions/module/lessonStore';
import withAuthorization from '../../../../Session/withAuthorization';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../../constants/global';
import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../../actions/breadcrumps/breadCrumps';
import PreLesson from './preLesson.js';
import VideoLesson from './videoLesson.js';
import ListenAndChoose from './listenandChoose.js';
import ListenAndRecord from './listenandRecord.js';
import SmartCoach from './smartCoach.js';
import FinalLesson from './finalLesson.js';


const Lessons = props => {
    //let breadcrumps = useSelector(selectBreadCrumpsPath);
    let history = useHistory();
    let dispatch = useDispatch();
    let lesson = props.lessonName;
    let lessonData = props.lessonData;
    let allLessonData = props.allModule;
    let modulePath = props.modulePath;
    
    const [currentLesson, setCurrentLesson] = useState(lessonData);
    const [lessonName, setLessonName] = useState(lesson);

     useEffect(() => {
       setCurrentLesson(lessonData)
    }, []);

    const next = (el) =>{
        let pos = allLessonData.map( (e)=> { return e.id; }).indexOf(el.id);

        if( allLessonData[pos].lesson_type == allLessonData[pos+1].lesson_type){
            setCurrentLesson(allLessonData[pos+1])
        }else{
            console.log("next..",history.location.pathname);
            redirectTo(modulePath+"/"+allLessonData[pos+1].lesson_type,{lessonData:allLessonData[pos+1], allModule:allLessonData, modulePath:modulePath})
            setCurrentLesson(allLessonData[pos+1]);
            dispatch(setBreadCrumpsPath( {"paths":history.location.pathname } ));
        }
    }

    const redirectTo = (path,data={}) => {
        if (path && path != "") {
          history.push({
            pathname: path,
            state: data
          })
        }
    }
    

    console.log("lessonData...",currentLesson);
    // console.log("allLessonData...",allLessonData)

    return (
        <>
           
            <div className="content-wrapper">
                {
                    (currentLesson.lesson_type == "preLesson")
                    ?
                        <PreLesson lessonName={currentLesson.lesson_type} lessonData = {currentLesson} next={(i)=>{next(i)}} />
                    :
                    (currentLesson.lesson_type == "guidedLesson")
                    ?  
                        (currentLesson.guided_lesson_type == "lesson")
                        ?
                            <VideoLesson lessonName={currentLesson.lesson_type} lessonData = {currentLesson} next={(i)=>{next(i)}} />
                        :
                        (currentLesson.guided_lesson_type == "listen_records")
                        ?
                            <ListenAndRecord lessonName={currentLesson.lesson_type} lessonData = {currentLesson} next={(i)=>{next(i)}} />
                        :
                        (currentLesson.guided_lesson_type == "listen_options")
                        ?
                            <ListenAndChoose lessonName={currentLesson.lesson_type} lessonData = {currentLesson} next={(i)=>{next(i)}} />
                        :
                        (currentLesson.guided_lesson_type == "recognising_tools")
                        ?
                            <SmartCoach lessonName={currentLesson.lesson_type} lessonData = {currentLesson} next={(i)=>{next(i)}} />
                        :
                            null
                    :(currentLesson.lesson_type=="finalAssignment")
                    ?
                        <FinalLesson  lessonName={currentLesson.lesson_type} lessonData = {currentLesson} next={(i)=>{next(i)}}/>    
                    :
                        null
                }
                    
            </div>
            
        </>
    )

}

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(Lessons);
