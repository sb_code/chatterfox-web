import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import StartMessage  from '../message/startMessage.js';

const VideoLesson= props=> {

    let history = useHistory();
    let dispatch = useDispatch();
    let lesson = props.lessonData;

    const [isStartMessage, setStartMessage] = useState( lesson.start_message?true:false );
    const [isEndMessage, setEndMessage] = useState( lesson.end_message?true:false );

    console.log("video lesson...",lesson)

    return(
        <>
            {isStartMessage?

                <StartMessage 
                            message ={ lesson.start_message } 
                            next = {(data)=>{setStartMessage(data)}}
                 />
                :

                <section className="introduction">
                    <div className="container">
                        <h3 className=""> {lesson.title?lesson.title:"title"}</h3>
                        <div className="video-lessons">
                            <iframe width="100%" height="450" src={lesson.file_url?lesson.file_url:"https://www.youtube.com/embed/xcJtL7QggTI"} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            <span className="text-center d-block">
                                {lesson.description?lesson.description:"descrition"}
                            </span>
                        </div>

                        <button type="submit" className="btn" onClick={()=>{props.next(lesson)}}>Next</button>
                    </div>
                </section>
            }
            
 
        </>
    )

}

export default VideoLesson;