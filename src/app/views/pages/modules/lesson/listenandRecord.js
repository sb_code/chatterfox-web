import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const ListenAndRecord= props=> {

    let history = useHistory();
    let dispatch = useDispatch();
    let lesson = props.lessonData;

    console.log("video lesson...",lesson)

    return(
        <>

            <section className="introduction">
                <div className="container">
                    <h3 className=""> {lesson.title?lesson.title:"title"}</h3>
                    <div className="video-lessons">
                        <iframe width="100%" height="450" src={lesson.file_url?lesson.file_url:"https://www.youtube.com/embed/xcJtL7QggTI"} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <span className="text-center d-block">
                            {lesson.description?lesson.description:"descrition"}
                        </span>
                    </div>

                    <button type="submit" className="btn">Next Video</button>
                </div>
            </section>
 
        </>
    )

}

export default ListenAndRecord;