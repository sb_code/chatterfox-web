import React, { useEffect, useState } from 'react';


const StartMessage = props => {
    let message = props.message;
    return (
        <>

           
        <section class="introduction">
            <div class="container">
                <div class="need-description-one text-center">
                    <div class="descrip-two start-msg">{message?message:"no message"}</div>
                </div>

                <button type="submit" class="btn" onClick={()=>{props.next(false)}}>Start</button>
            </div>
        </section>
              
        </>
    )

}

export default StartMessage;