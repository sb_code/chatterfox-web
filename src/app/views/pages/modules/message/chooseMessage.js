import React, { useEffect, useState } from 'react';


import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';

const ChooseMessage = props => {

    return (
        <>
            <Header title="Choose Message"/>
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="introduction">
                        <div class="container">

                            <div class="need-description-one">
                                <div class="descrip-two text-left listen-choose-heading">Result</div>
                            </div>

                            <div class="need-description-one">
                                <div class="descrip-two start-msg"><strong class="d-block">6 /10</strong><span class="d-block">Lorem ipsum dolor sit amet consectetur</span></div>
                            </div>

                            <button type="submit" class="btn btn-activity">Try Again</button>

                            <div class="need-description-one mt-3">
                                <div class="descrip-two start-msg" disabled>Ready for some feedback?<br />Your Smart coach is going to give you some!</div>
                            </div>

                            <button type="submit" class="btn">Let's Start</button>

                        </div>
                    </section>
                </div>


            </div>
        </>
    )

}

export default ChooseMessage;