import React, { useEffect, useState } from 'react';


import Header from '../../layouts/Header.js';
import Menu from '../../layouts/Menu.js';

const EndMessage = props => {

    return (
        <>
            <Header title="Message"/>
            <Menu />
            <div class="app-content content">
                <div class="content-wrapper">
                    <section class="introduction">
                        <div class="container">
                            <div class="need-description-one text-center">
                                <div class="descrip-two start-msg">Great job!</div>
                                <div class="descrip-two start-msg">Now it's time to really sound like a native!</div>
                            </div>

                            <button type="submit" class="btn">Next</button>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )

}

export default EndMessage;