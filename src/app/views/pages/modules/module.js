import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectUser, setUser } from '../../../actions/user/userSlice';
import { selectModule, setModules } from '../../../actions/module/moduleSlice';
import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../actions/breadcrumps/breadCrumps';
import { logOut } from '../../../services/authServices';
import { getModuleList } from '../../../services/moduleServices';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../constants/global';
import { get, post } from '../../../util/httpClient';
import withAuthorization from '../../../Session/withAuthorization';
import { setData, getData } from '../../../util/localStorageHandler.js';

// import GridView from './gridView.js';
// import ListView from './listView.js';
import ModuleView from './moduleDetails/moduleView.js';
import Header from '../../layouts/Header';
import Menu from '../../layouts/Menu';
// import PropTypes from 'prop-types';

const Modules = props => {
  const module = useSelector(selectModule);
  let breadcrumps = useSelector(selectBreadCrumpsPath);
  let history = useHistory();
  let dispatch = useDispatch();


  const [authToken, setAuthToken] = useState("");
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [userId, setUserId] = useState("");
  const [othModules, setOthModules] = useState([]);
  const [introModules, setIntroModules] = useState([]);
  const [trialModules, setTrialModules] = useState([]);
  const [view, setView] = useState("grid");

  useEffect(() => {
    let data = getData();
    if (data) {
      getModuleData(data.api_token);
      setAuthToken(data.api_token);
    }

    //dispatch(setBreadCrumpsPath( {"paths":"modules_0","index":0,"route":"modules", "payload":{}} ));

    // console.log("value of modules in global store ==", data.modules);
  }, []);

  useEffect(() => {
    if (module && module.modules) {
      module.modules && setOthModules(module.modules);
      module.introModules && setIntroModules(module.introModules);
      module.trialModules && setTrialModules(module.trialModules);
      // console.log("value of modules in global store after update ==", module.modules);
    }
  }, [module, setOthModules, setTrialModules, setIntroModules]);

  const getModuleData = async (token) => {
    setLoading(true);
    getModuleList(token).then(res => {
      //console.log("Response from get module list ==", res);
      if (res && !res.isError && !res.errorCode) {

        setLoading(false);
        res.module_list && setOthModules(res.module_list);
        res.intro_modules && setIntroModules(res.intro_modules);
        res.trial_modules && setTrialModules(res.trial_modules);
        let payload = {
          'othModules': res.module_list ? res.module_list : [],
          'introModules': res.intro_modules ? res.intro_modules : [],
          'trialModules': res.trial_modules ? res.trial_modules : []
        };
        dispatch(setModules(payload));

      } else {
        setLoading(false);
      }
    })


  }


  const changeView = (status) => {
    if (status) {
      setView(status);
    }
  }

  return (

    <>

      <div class="content-wrapper">
        <section class="unlock-membership">
          <div class="container">
            <div class="unlock-bg">
              <div class="figcaption">
                <h1>Get Fluent Faster!</h1>
                <p>Unlock Your Membership!</p>
              </div>

              <a href="JavaScript:Void(0);">I'm Ready</a>
            </div>
          </div>
        </section>
      </div>

      <section class="introduction-module-list sec-pad">
        <div class="container">
          <div class="list-icon text-right">
            <a href="JavaScript:Void(0);" class={view == "list" ? "active" : ""} onClick={() => changeView('list')}>
              <i class="fa fa-list"></i>
            </a>
            <a href="JavaScript:Void(0);" class={view == "grid" ? "active" : ""} onClick={() => changeView('grid')}>
              <i class="fa fa-th"></i>
            </a>
          </div>

          {/* component section- grid/list */}
          <ModuleView gridType={view} />
          {/* component section- grid/list */}

        </div>
      </section>

    </>

  );
};

// modules.propTypes = {

// };

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(Modules);