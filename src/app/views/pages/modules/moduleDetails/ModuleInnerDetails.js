import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
//import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../../actions/breadcrumps/breadCrumps';
//import { selectLessonStore, setLessonStore } from '../../../../actions/module/lessonStore';

const ModuleInnerDetails = props => {
  	let history = useHistory();
    //let lessons = useSelector(selectLessonStore);
    let allModule =  props.allModule;
    let modulePath = props.modulePath;
  	
    const [state, setState] = useState({
      title: props.title,
      module: props.module,
      //allModule: props.allModule,
      type : props.type,
      path: props.path
    });

  	//let breadcrumps = useSelector(selectBreadCrumpsPath);
  	let dispatch = useDispatch();

  	const redirectTo = (path,data={}) => {
        if (path && path != "") {
          history.push({
            pathname: path,
            state: data
          })
        }
    }


    console.log("ModuleInnerDetails...",allModule)

    return (
      <div className="module-details">
        <div className="small-heading">
            <h2>{state.title}</h2>
        </div>
        <div className="row">
            {state.module.map((elm,ind)=>{

              let title = elm.title;
              if(state.type == "guided_lesson"){

                if(elm.guided_lesson_type != "lesson"){
                  title = elm.guided_lesson_type.replace("_", " ");
                  //make title camelcasing
                  title = title.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase()});
                }
              }

            	let imgIcon = elm.icon_url?elm.icon_url:"images/md-ic1.png";
            	let imgGrayIcon = elm.gray_icon_url?elm.gray_icon_url:"images/md-ic1.png";

                if(state.module.length == 1){

                     return   <a 
                                  href="JavaScript:void(0);"  
                                  onClick={() => redirectTo(state.path,{lessonData:elm, allModule:allModule, modulePath:modulePath})} 
                                  className="col-lg-4"  
                                  key={state.title+ind}
                              >
                                
                                <div className="module-bg-sec text-center">
                                    <div className="figure">
                                        <img className="col-sm-12 p-0" src={elm.is_completed?imgIcon:imgGrayIcon} />
                                    </div>
                                    <span className="d-block">{title?title:"Title"}</span>
                                </div>
                                
                                {elm.is_completed?
                                    <div className="feedback"><button className="btn-one">Check your Coach's APM Feedback</button><button href="#" className="btn-one btn-two">Show Feedback</button></div>
                                :null}
                            </a>
                }else{

                    if(elm.is_completed){

                        return  <a 
                                    href="JavaScript:void(0);"  
                                    onClick={() => redirectTo(state.path,{lessonData:elm, allModule:allModule, modulePath:modulePath})} 
                                    className="col-lg-4"  key={state.title+ind}
                                >
                                    <div className="module-bg-sec text-center">
                                        <div className="figure">
                                            <img className="col-sm-12 p-0" src={imgIcon} />
                                        </div>
                                        <span className="d-block">{title?title:"Title"}</span>
                                    </div>
                                     <div className="feedback"><button className="btn-one">Check your Coach's APM Feedback</button><button href="#" className="btn-one btn-two">Show Feedback</button></div>
                                </a>
                                   
                    }else{

                    	if(ind != 0){

                    		if(state.module[ind-1].is_completed){

	                            <a 
                                href="JavaScript:void(0);"  
                                onClick={() => redirectTo(state.path,{lessonData:elm, allModule:allModule, modulePath:modulePath})} 
                                className="col-lg-4"  key={state.title+ind}
                              >

	                                <div className="module-bg-sec text-center">
	                                    <div className="figure">
	                                       <img className="col-sm-12 p-0" src={elm.is_completed?imgIcon:imgGrayIcon} />
	                                    </div>
	                                    <span className="d-block">{title?title:"Title"}</span>
	                                </div>

	                            </a>
	                        }else{
	                            return  <div className="col-lg-4"  key={state.title+ind}>
	                                    <div className="module-bg-sec module-bg-light text-center">
	                                        <div className="figure">
	                                            <img className="col-sm-12 p-0" src={elm.is_completed?imgIcon:imgGrayIcon} />
	                                        </div>
	                                        <span className="d-block">{title?title:"Title"}</span>
	                                    </div>
	                                   
	                                </div>
	                        }

                    	}else{

                    		 return  <a 
                                   href="JavaScript:void(0);"  
                                   onClick={() => redirectTo(state.path,{lessonData:elm, allModule:allModule, modulePath:modulePath})} 
                                   className="col-lg-4"  key={state.title+ind}
                                  >
	                                    <div className="module-bg-sec text-center">
	                                        <div className="figure">
	                                            <img className="col-sm-12 p-0" src={elm.is_completed?imgIcon:imgGrayIcon} />
	                                        </div>
	                                        <span className="d-block">{title?title:"Title"}</span>
	                                    </div>
	                                   
	                                </a>

                    	}
                        
                        
                    }
                }
            })}
        </div>
    </div>
   )
};

// ModuleInnerDetails.propTypes = {

// };

export default ModuleInnerDetails;