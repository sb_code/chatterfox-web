import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectUser, setUser } from '../../../../actions/user/userSlice';
import { selectModule, setModules } from '../../../../actions/module/moduleSlice';
//import { selectLessonStore, setLessonStore } from '../../../../actions/module/lessonStore';
//import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../../actions/breadcrumps/breadCrumps';
import { logOut } from '../../../../services/authServices';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../../constants/global';
import { get, post } from '../../../../util/httpClient';
import withAuthorization from '../../../../Session/withAuthorization';
import { setData, getData } from '../../../../util/localStorageHandler.js';
import { getModuleDetailService } from '../../../../services/moduleServices';

import Header from '../../../layouts/Header.js';
import Menu from '../../../layouts/Menu.js';
import ModuleInnerDetails from './ModuleInnerDetails'

const ModuleDetails = props => {

    //let breadcrumps = useSelector(selectBreadCrumpsPath);
    let history = useHistory();
    let dispatch = useDispatch();
    let allModulesData = [];
    //let moduleData = props.location.state?props.location.state:{"id":props.location.pathname.split("/")[2]};
    let moduleData = {"id":props.modulesId};
    const [authToken, setAuthToken] = useState("");
    const [loading, setLoading] = useState(false);
    const [moduleDetailsData, setModuleDetailsData] = useState({});
    const [guided_lesson, setGuidedLesson] = useState([]);
    const [pre_lesson, setPreLesson] = useState([]);
    const [fluency_activity, setFluencyActivity] = useState([]);
    const [final_assignment, setFinalAssignment] = useState([]);
    const [allModule, setAllModule] = useState({});

    useEffect(() => {
        let data = getData();
        if (data && moduleData) {
          getModuleDetails(data.api_token,moduleData.id);
          setAuthToken(data.api_token);
          //dispatch(setBreadCrumpsPath( {"paths":moduleData.title+"_1","index":1,"route":"moduleDetails", "payload":{id:moduleData.id,title:moduleData.title} } ));
        } 
    }, []);

    const getModuleDetails = async (token,id) => {
        setLoading(true);
        getModuleDetailService(token,id).then(res=>{

            if (!res.isError && !res.errorCode) {
         
              setLoading(false);
              
              setModuleDetailsData(res.module);

              generatePreLessonArray(res.module);
              generateGuidedLessonArray(res.module);
              generateFluencyActivityArray(res.module);
              generateFinalAssignmentArray(res.module);
              
              //dispatch(setLessonStore(allModulesData));

              setAllModule(allModulesData);

            } else {
              setLoading(false);
            }

        }) 
    }

   

    const generatePreLessonArray = (module) =>{

        let temArr = [];
        let pre_lesson = module.prelessson ?  module.prelessson : [];
 
        for(let i=0; i < pre_lesson.prelessons.length; i++ ){

            //push start and completion message to lesson array
            if(i == 0){
                pre_lesson.prelessons[i].start_message = pre_lesson.pre_lesson_start_msg;
            }
            if(i == pre_lesson.prelessons.length - 1 ){
                pre_lesson.prelessons[i].end_message = pre_lesson.pre_lesson_completion_msg;
            }

            temArr.push(pre_lesson.prelessons[i]);

            pre_lesson.prelessons[i].lesson_type = "preLesson";

            allModulesData.push(pre_lesson.prelessons[i]);
        }

        setPreLesson(temArr);
        
    }

    const generateFluencyActivityArray = (module) =>{
        let temArr = [];
        let fluency_lesson = module.fluency_activity ?  module.fluency_activity : [];
 
        for(let i=0; i < fluency_lesson.fluency_activities.length; i++ ){

            //push start and completion message to lesson array
            if(i == 0){
                fluency_lesson.fluency_activities[i].start_message = fluency_lesson.fluency_activities_start_message;
            }
            if(i == fluency_lesson.fluency_activities.length - 1 ){
                fluency_lesson.fluency_activities[i].end_message = fluency_lesson.fluency_activities_completion_msg;
            }

            temArr.push(fluency_lesson.fluency_activities[i]);

            fluency_lesson.fluency_activities[i].lesson_type = "fluencyActivity";
            allModulesData.push(fluency_lesson.fluency_activities[i]);
        }

        setFluencyActivity(temArr);

    }

    const generateFinalAssignmentArray = (module) =>{
        let temArr = [];
        let final_lesson = module.final_assignment ?  module.final_assignment : [];

        setFinalAssignment(final_lesson);
        final_lesson.lesson_type = "finalAssignment";
        allModulesData.push(final_lesson);

    }

    const generateGuidedLessonArray = (module) =>{
        let temArr = [];
        let guided_lesson = module.guided_lesson;

        //video_lessons >> lessons
        for(let i=0; i < guided_lesson.video_lessons.lessons.length; i++ ){

            //push start and completion message to lesson array
            if(i == 0){
                guided_lesson.video_lessons.lessons[i].start_message = guided_lesson.video_lessons.video_lesson_start_msg;
            }
            if(i == guided_lesson.video_lessons.lessons.length - 1 ){
                guided_lesson.video_lessons.lessons[i].end_message = guided_lesson.video_lessons.video_lesson_completion_msg;
            }

            temArr.push(guided_lesson.video_lessons.lessons[i]);
            guided_lesson.video_lessons.lessons[i].lesson_type = "guidedLesson";
            allModulesData.push(guided_lesson.video_lessons.lessons[i]);
        }
        
        //listen_and_choose_correct_options
        for(let i=0; i < guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options.length; i++ ){
            
            //push start and completion message to lesson array
            if(i == 0){ 
                guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options[i].start_message = guided_lesson.listen_and_choose_correct_option.listen_choose_start_msg;
            }
            if(i == guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options.length - 1 ){
                guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options[i].end_message = guided_lesson.listen_and_choose_correct_option.listen_option_result_msg;
            }
            
            temArr.push(guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options[i]);
            guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options[i].lesson_type = "guidedLesson";
            allModulesData.push(guided_lesson.listen_and_choose_correct_option.listen_and_choose_correct_options[i]);
        }

        //lesson_and_records
        for(let i=0; i < guided_lesson.listen_and_records.lesson_and_records.length; i++ ){

            //push start and completion message to lesson array
            if(i == 0){
                guided_lesson.listen_and_records.lesson_and_records[i].start_message = guided_lesson.listen_and_records.listen_record_start_msg;
            }
            if(i == guided_lesson.listen_and_records.lesson_and_records.length - 1 ){
                guided_lesson.listen_and_records.lesson_and_records[i].end_message = guided_lesson.listen_and_records.listen_record_completion_msg;
            }

            temArr.push(guided_lesson.listen_and_records.lesson_and_records[i]);
            guided_lesson.listen_and_records.lesson_and_records[i].lesson_type = "guidedLesson";
            allModulesData.push(guided_lesson.listen_and_records.lesson_and_records[i]);
        }

        //recognising_tools
        for(let i=0; i < guided_lesson.recognising_tools.recognising_tools.length; i++ ){

            //push start and completion message to lesson array
            if(i == 0){
                guided_lesson.recognising_tools.recognising_tools[i].start_message = guided_lesson.recognising_tools.recognising_tool_start_msg;
            }
            if(i == guided_lesson.recognising_tools.recognising_tools.length - 1 ){
                guided_lesson.recognising_tools.recognising_tools[i].end_message = guided_lesson.recognising_tools.recognising_tool_completion_msg;
            }

            temArr.push(guided_lesson.recognising_tools.recognising_tools[i]);
            guided_lesson.recognising_tools.recognising_tools[i].lesson_type = "guidedLesson";
            allModulesData.push(guided_lesson.recognising_tools.recognising_tools[i]);
        }   

        setGuidedLesson(temArr);
        
    }

    console.log("pre_lesson..",pre_lesson)

    console.log("allModulesData 2...",allModule);
            

    return (
        <>
           
            <div className="content-wrapper bg-invite">
                <section className="invite-friends">
                    <div className="container-fluid">
                        <div className="row align-items-center">
                            <div className="col-lg-6">
                                <div className="figure">
                                    {moduleDetailsData.photo?
                                    <img src={moduleDetailsData.photo} />
                                    :
                                    <img src="images/md-details.png" />
                                    }
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="figcaption text-center">
                                    <h1>{moduleDetailsData.description?moduleDetailsData.description:"Now that you know how ChatterFox work, You're going to learn some mind-blowing facts about fluency..."}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <section className="module-details-outer-sec sec-pad">

                <div className="container">

                    {pre_lesson.length?
                            <ModuleInnerDetails
                                title="Pre Lesson"
                                type="pre_lesson"
                                allModule = {allModule}
                                module={pre_lesson}
                                modulePath={props.path}
                                path = {props.path+"/preLesson"}
                            />
                    :null}

                    {guided_lesson.length?
                        <ModuleInnerDetails
                                title="Guided Lesson"
                                type="guided_lesson"
                                allModule = {allModule}
                                module={guided_lesson}
                                modulePath={props.path}
                                path = {props.path+"/guidedLesson"}
                            />
                    :null}

                    {fluency_activity.length?
                            <ModuleInnerDetails
                                title="Fluency Activity"
                                type="fluency_activity"
                                allModule = {allModule}
                                module={fluency_activity}
                                modulePath={props.path}
                                path = {props.path+"/fluencyActivity"}
                            />
                    :null}

                    {final_assignment.length?
                        <ModuleInnerDetails
                                title="Final Assessment"
                                type="final_assignment"
                                allModule = {allModule}
                                module={final_assignment}
                                modulePath={props.path}
                                path = {props.path+"/finalAssessment"}
                            />
                    :null}
                    <span className="text-center d-block">

                        <button className="btn btn-red btn-orange" type="submit">Go to Review Page</button>
                       
                    </span>
                </div>
            </section>
         
        </>
    )

}

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(ModuleDetails);

