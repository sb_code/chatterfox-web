import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectModule, setModules } from '../../../../actions/module/moduleSlice';
import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../../actions/breadcrumps/breadCrumps';
import { logOut } from '../../../../services/authServices';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../../constants/global';
import { get, post } from '../../../../util/httpClient';
import  CircularProgressBar from '../../../components/CircularProgressBar';

const ModuleView = props => {

    const module = useSelector(selectModule);
    let breadcrumps = useSelector(selectBreadCrumpsPath);
    let history = useHistory();
    let dispatch = useDispatch();
    let gridType = props.gridType? props.gridType:"grid";

    const [details, setDetails] = useState([]);
    const [othModules, setOthModules] = useState([]);
    const [introModules, setIntroModules] = useState([]);
    const [trialModules, setTrialModules] = useState([]);

    useEffect(() => {
        if(module){
            // setDetails(props.data);
            module.othModules && setOthModules(module.othModules);
            module.introModules && setIntroModules(module.introModules);
            module.trialModules && setTrialModules(module.trialModules);
        }

    }, []);

    useEffect(() => {
        if(module){
            module.othModules && setOthModules(module.othModules);
            module.introModules && setIntroModules(module.introModules);
            module.trialModules && setTrialModules(module.trialModules);
        }
    }, [module, setOthModules, setIntroModules, setTrialModules]);


    const redirectTo = (path,data={}) => {

        if (path && path != "") {
          history.push({
            pathname: path,
            state: data
          })
        }
    }

    //console.log("module ",module);

    return (
        <div className={ gridType=="list"?"tab-section for-list-view":"tab-section"}>
            <div id="accordion" className="accordion">
                 {module.introModules.length?
                    <div className="card mb-0">
                   
                        <div className="card-header collapse" data-toggle="collapse" href="#collapseOne">
                            <a className="card-title">Introduction Module</a>

                        </div>
                        <div id="collapseOne" className="card-body collapse show" data-parent="">
                            <div className="row">
                                {module.introModules.map((elm,ind)=>{
                                    if(elm.is_unlocked){
                                        return  <div className={ gridType=="list"?"col-lg-12":"col-lg-4"} key={"intro"+ind}>
                                                    <div className="tab-bg phase">
                                                        <ul>
                                                            <li>
                                                                <figure>
                                                                    <img className="iconImage" src={elm.icon_url} />
                                                                </figure>
                                                            </li>
                                                            <li className="pl-1">
                                                                <strong>{elm.title}</strong>
                                                                <p>This Module is not available</p>
                                                            </li>
                                                            <li>
                                                            <CircularProgressBar
                                                                strokeWidth="2"
                                                                sqSize="60"
                                                                percentage={elm.user_progress}
                                                            />
                                                                
                                                            </li>
                                                        </ul>
                                                    </div>
                                            </div>
                                        }else{
                                            return  <a  
                                                        href="JavaScript:void(0);" 
                                                        onClick={() => redirectTo(`/modules/IntroductionModule_${elm.id}`,{id:elm.id,title:"Introduction Module"})} 
                                                        className={ gridType=="list"?"col-lg-12 mb-2":"col-lg-4"} 
                                                        key={"intro"+ind}
                                                    >
                                                        <div className="tab-bg">
                                                            <ul>
                                                                <li>
                                                                    <figure>
                                                                        <img className="iconImage" src={elm.icon_url} />
                                                                    </figure>
                                                                </li>
                                                                <li className="pl-1">
                                                                    <strong>{elm.title}</strong>
                                                                    <p>This Module is available</p>
                                                                </li>
                                                                <li>
                                                                 <CircularProgressBar
                                                                        strokeWidth="2"
                                                                        sqSize="60"
                                                                        percentage={elm.user_progress}
                                                                    />
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                        }
                                    
                             })}
                                

                            </div>
                        </div>
                    </div>
                :null}
                {module.trialModules.length?
                <div className="card mb-0">
                    <div className="card-header collapse" data-toggle="collapse" data-parent="" href="#collapseTwo">
                        <a className="card-title">Trial Module</a>

                    </div>
                    <div id="collapseTwo" className="card-body collapse show" data-parent="">
                        <div className="row">
                           {module.trialModules.map((elm,ind)=>{
                                if(elm.is_unlocked){
                                    return  <div className={ gridType=="list"?"col-lg-12 mb-2":"col-lg-4"} key={"trial"+ind}>
                                                <div className="tab-bg phase">
                                                    <ul>
                                                        <li>
                                                            <figure>
                                                                <img className="iconImage" src={elm.icon_url} />
                                                            </figure>
                                                        </li>
                                                        <li className="pl-1">
                                                            <strong>{elm.title}</strong>
                                                            <p>This Module is not available</p>
                                                        </li>
                                                        <li>
                                                           <CircularProgressBar
                                                                        strokeWidth="2"
                                                                        sqSize="60"
                                                                        percentage={elm.user_progress}
                                                                    />
                                                        </li>
                                                    </ul>
                                                </div>
                                        </div>
                                    }else{
                                        return  <a href="JavaScript:void(0);" onClick={() => redirectTo(`/modules/TrialModule_${elm.id}`,{id:elm.id,title:"Trial Module"})} class={ gridType=="list"?"col-lg-12 mb-2":"col-lg-4"} key={"intro"+ind}>
                                                    <div className="tab-bg">
                                                        <ul>
                                                            <li>
                                                                <figure>
                                                                    <img className="iconImage" src={elm.icon_url} />
                                                                </figure>
                                                            </li>
                                                            <li className="pl-1">
                                                                <strong>{elm.title}</strong>
                                                                <p>This Module is available</p>
                                                            </li>
                                                            <li>
                                                                <CircularProgressBar
                                                                        strokeWidth="2"
                                                                        sqSize="60"
                                                                        percentage={elm.user_progress}
                                                                    />
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </a>
                                    }
                                
                         })}

                            
                        </div>
                    </div>
                </div>
                :null}
                {module.othModules.length?
                    module.othModules.map((otherElm,ind)=>{
                        return <div className="card mb-0"  key={"other"+ind}>
                                    <div className="card-header collapse" data-toggle="collapse" data-parent="" href={"#collapse"+ind}>
                                        <a className="card-title">{otherElm.title}</a>

                                    </div>
                                    <div id={"collapse"+ind} className="collapse show" data-parent="">
                                        <div className="card-body">
                                            <div className="row">
                                                {otherElm.modules.map((elm,ind)=>{
                                                    if(elm.is_unlocked){
                                                        return <div className={ gridType=="list"?"col-lg-12 mb-2":"col-lg-4"}>
                                                                <div className="tab-bg phase">
                                                                    <ul>
                                                                        <li>
                                                                            <figure className="bg-red">
                                                                                <img className="iconImage" src={elm.icon_url} />
                                                                            </figure>
                                                                        </li>
                                                                        <li className="pl-1">
                                                                            <strong>{elm.title}</strong>
                                                                            <p>This Module is not available</p>
                                                                        </li>
                                                                        <li>
                                                                            <CircularProgressBar
                                                                                strokeWidth="2"
                                                                                sqSize="60"
                                                                                percentage={elm.user_progress}
                                                                            />
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                
                                                            </div>
                                                    }else{
                                                        return <a href="JavaScript:void(0);" onClick={() => redirectTo(`/modules/${otherElm.title.trim().replace(/\s+/g, '')+"_"+elm.id}`,{id:elm.id,title:otherElm.title})} class={ gridType=="list"?"col-lg-12 mb-2":"col-lg-4"} key={"other"+ind}>
                                                                <div className="tab-bg">
                                                                    <ul>
                                                                        <li>
                                                                            <figure className="bg-red">
                                                                                <img className="iconImage" src={elm.icon_url} />
                                                                            </figure>
                                                                        </li>
                                                                        <li className="pl-1">
                                                                            <strong>{elm.title}</strong>
                                                                            <p>This Module is available</p>
                                                                        </li>
                                                                        <li>
                                                                            <CircularProgressBar
                                                                                strokeWidth="2"
                                                                                sqSize="60"
                                                                                percentage={elm.user_progress}
                                                                            />
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div className="feedback">
                                                                    <button className="btn-one">Check your Coach's APM Feedback</button>
                                                                    <button className="btn-one btn-two">Show Feedback</button>
                                                                </div>
                                                            </a>
                                                    }
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        })
                :null}  
            </div>
        </div>
    );

};

export default ModuleView;