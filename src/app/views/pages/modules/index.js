import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { selectUser, setUser } from '../../../actions/user/userSlice';
import { selectModule, setModules } from '../../../actions/module/moduleSlice';
import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../../actions/breadcrumps/breadCrumps';
import { logOut } from '../../../services/authServices';
import { API_BASEURL, API_BASEURL_STAG, CF_USER } from '../../../constants/global';
import { get, post } from '../../../util/httpClient';
import withAuthorization from '../../../Session/withAuthorization';
import { setData, getData } from '../../../util/localStorageHandler.js';
//import { useParams } from 'react-router';

import Modules from './module.js';
import ModuleDetails from './moduleDetails/moduleDetails.js';
import Lessons from './lesson/lesson';
import Header from '../../layouts/Header';
import Menu from '../../layouts/Menu';


const ModuleIndex = props => {
  const module = useSelector(selectModule);
  let breadcrumps = useSelector(selectBreadCrumpsPath);
  let history = useHistory();
  let dispatch = useDispatch();
  let path = props.location.pathname;
  let currentPath = path.split("/");
  
  const [state, setState] = useState({
      renderPage:""
  });


  const [authToken, setAuthToken] = useState("");
  
  
  console.log("currentPath....",currentPath);

  // history.listen((location, action) => {
  //     console.log("on route change",location);
  //     init(location)
  // });

  useEffect(() => {

    init(props.location)

  }, []);

  const init =(location) =>{
    /****** fetch user data from local storage *******/
    let data = getData();
    /*******/

    switch (currentPath.length) {
      case 2:
        setState({ renderPage: <Modules/> });
        break;
      case 3:
        
        if(!location.state){
           history.push({
            pathname: "/modules",
            state:{} 
          })
          return false;
        }

        setState({ renderPage:  <ModuleDetails 
                                  modulesId = {location.state.id?location.state.id:currentPath[2].split("_")[1]} 
                                  path={path}
                                /> 
                });
        break;
      case 4:
       
        if(!location.state){
          history.push({
            pathname: "/modules",
            state: {}
          })
          return false;
        }

        console.log("hhh",location.state.lessonData)

        setState({ renderPage: <Lessons 
                                  lessonName={currentPath[3]} 
                                  lessonData={location.state.lessonData} 
                                  allModule={location.state.allModule} 
                                  modulePath={location.state.modulePath} 
                              />
                          });
     
    }
    
    if (data) {

      /****** Set to localdtorage user token data *******/
      setAuthToken(data.api_token);
      /*******/
      
      dispatch(setBreadCrumpsPath( {"paths":path } ));

    }
  }


  return (
  
    <>
      <Header title="Modules" isBreadcrumps={true} />
      <Menu />

      <div class="app-content content">

        {state.renderPage?
          state.renderPage
          :null}

      </div>
    </>

  );
};

// modules.propTypes = {

// };

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(ModuleIndex);