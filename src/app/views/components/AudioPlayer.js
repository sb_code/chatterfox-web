import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import ReactAudioPlayer from 'react-audio-player'

let rap;

const AudioPlayer = props => {

    const [state, setState] = useState({
      source: props.source,
      isPlaying: false
    });
    useEffect(() => {
        

    }, []);

    console.log("audioPLayer..",rap)

    return (
        <div className="col-sm-12 p-0">    
            <div className="intro-video-sec">
                <ReactAudioPlayer
                  src={state.source}
                  ref={(element) => { rap = element; }}
                  autoPlay = {false}
                  controls = {false}
                  onPlay={()=>{setState({ isPlaying: true }) }}
                  onEnded={()=>{setState({ isPlaying: false }) }}
                  onPause={()=>{setState({ isPlaying: false }) }}
                />
                <ul>
                    <li>
                        {!state.isPlaying?
                            <a className="theme-circle" href="JavaScript:void(0);"  
                                onClick={() => console.log(rap.audioEl.current.play()) } 
                            >
                                <img src={process.env.PUBLIC_URL + '/images/play.png'} />
                            </a>
                        : 
                            <a className="theme-circle" href="JavaScript:void(0);"  
                                onClick={() => console.log(rap.audioEl.current.pause()) } 
                            >
                                <img src={process.env.PUBLIC_URL + '/images/pause.png'} />
                            </a>
                        }
                       
                    </li>
                    <li>
                        <strong className="d-block">First Recorded Voice</strong>
                        {/*<span className="d-block">Duration: 00:00</span>*/}
                    </li>
                    <li><img src={process.env.PUBLIC_URL + '/images/rc-ic.png' } /></li>
                </ul>
            </div>
            
        </div>
    );

};

export default AudioPlayer;