import React from 'react';

const ErrorTextInline = props => {
  return (
    <p class="text-center warning" style={props.style?props.style:{}}>{props.text}</p>
  );
};

export default ErrorTextInline;