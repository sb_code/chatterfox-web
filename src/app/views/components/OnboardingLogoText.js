import React from 'react';

const OnboardingLogoText = (props) => {
  return (
    <>
      <div class="figure d-block text-center">
        <img src="images/logo.png" />
      </div>
      <div class="figcaption text-center">
        <div class="site-navbar d-block mb-2">
          <h2>{props.text}</h2>
          {props.subText&&<p>{props.subText}</p>}
        </div>
      </div>
    </>
  );
};

export default OnboardingLogoText;