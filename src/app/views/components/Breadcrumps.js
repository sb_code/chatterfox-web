import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const BreadcrumpsComp = props => {
	
	let history = useHistory();
    //let dispatch = useDispatch();
    let breadcrumps = props.breadcrumps;
    let isBreadcrumps = props.isBreadcrumps;
    let title = props.title

    const cleanUrlPath = (path) =>{
	    let clearPath = path.split("").reverse().join("").replace("/","").split("").reverse().join("");
	    return clearPath;
	    console.log("clearPath",clearPath)
	}

	const redirectTo = (path,data={}) => {
        path = cleanUrlPath(path);
        
        if (path && path != "") {
          history.push({
            pathname: path,
            state: data
          })
        }
	  }

  return (
    <>
    	{breadcrumps.paths.length > 1?
                <a  href="JavaScript:void(0);"  onClick={()=>{history.goBack()}} className="pr-1" ><img width="50" src={process.env.PUBLIC_URL + '/images/left-arrow.png'} /></a>
              :null}
    	{isBreadcrumps && breadcrumps.paths.length
                ?
          breadcrumps.paths.map((elm,ind)=>{
            
            if(ind == breadcrumps.paths.length-1){
              if(elm.search("_") > 0){
                elm = elm.split("_")[0];
              }

              // make text visibilly good, space on camel casing
              let linktext = elm.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase()});
              
              return <h2>{linktext}</h2>
            
            }else{
              if(elm.search("_") > 0){
                elm = elm.split("_")[0];
              }

              // make text visibilly good, space on camel casing
              let linktext = elm.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase()});
              
              return <h2><a href="JavaScript:void(0);" onClick={() => redirectTo(breadcrumps.route[ind], elm.payload )} >{ linktext }</a><span className="p-1">/</span></h2>
            
            }
            
          })
        :
          <h2>{title}</h2>
        }
    </>
  );
};

// BreadcrumpsComp.propTypes = {

// };

export default BreadcrumpsComp;