import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const SubmitButtonBig = props => {
  if (props.isLoading) {
    return (
      <button type="button" disabled class="btn-red btn-orange disable">{props.caption} <FontAwesomeIcon icon={faSpinner} /></button>
    )
  } else {
    return (
      <button type="button" onClick={() => props.onClick()} class="btn-red btn-orange">{props.caption}</button>
    )
  }
};

// SubmitButtonBig.propTypes = {

// };

export default SubmitButtonBig;