import React from 'react';
// import PropTypes from 'prop-types';

const BackButton = props => {
  return (
    <div class="left-arrow" onClick={()=>props.onClick()}>
      <a href="javascript:void(0);"><i class="fa fa-arrow-left"></i></a>
    </div>
  );
};

// BackButton.propTypes = {

// };

export default BackButton;