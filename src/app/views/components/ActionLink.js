import React from 'react';
// import PropTypes from 'prop-types';

const ActionLink = props => {
  return (
    <strong><a href="javascript:void(0);" onClick={() => props.onClick()}>{props.caption}</a></strong>
  );
};

// ActionLink.propTypes = {

// };

export default ActionLink;