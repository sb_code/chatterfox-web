import React, { useState, useEffect } from 'react';
// import PropTypes from 'prop-types';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { CF_USER } from '../../constants/global';

import { selectUser } from '../../actions/user/userSlice';
import { logOut } from '../../services/authServices';
import withAuthorization from '../../Session/withAuthorization';
import { selectBreadCrumpsPath, setBreadCrumpsPath } from '../../actions/breadcrumps/breadCrumps';
import BreadcrumpsComp from '../components/Breadcrumps';

const Header = props => {

  const user = useSelector(selectUser);
  let breadcrumps = useSelector(selectBreadCrumpsPath);
  const dispatch = useDispatch();
  let history = useHistory();

  const [searchKey, setSearchKey] = useState('');
  const [searching, setSearching] = useState(false);
  const [userId, setUserId] = useState('');
  const [userName, setUserName] = useState('');
  const [photo, setPhoto] = useState('');

  useEffect(() => {
    // console.log("Logged In user details ===", user);
    setUserId(user.id);
    setUserName(user.username);
    setPhoto(user.photo_url);
    setSearchKey(user.searchKey);
  });

  useEffect(()=>{
    setUserId(user.id);
    setUserName(user.username);
    setPhoto(user.photo_url);
    setSearchKey(user.searchKey);
  },[user, setUserId, setUserName, setPhoto, setSearchKey]);

  
  //console.log("breadcrumps...",breadcrumps)

  return (
    <nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="navbar-collapse" id="navbar-mobile">
            <div class="float-left bookmark-wrapper d-flex align-items-center">
              <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu d-xl-none mr-auto">
                  <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="JavaScript:Void(0);"> <i class="ficon bx bx-menu"></i></a>
                </li>
              </ul>
            </div>

            <nav class="site-navbar">
              
              <BreadcrumpsComp
                isBreadcrumps       = {props.isBreadcrumps}
                breadcrumps         = {breadcrumps}
                title               = {props.title}
              />

              <ul class="ml-auto">
                <li class="search-sec">
                  {/* <form class="form-inline">
                    <input type="text" class="form-control" value={searchKey} id="myInput" placeholder="Search" title="" />
                  </form> */}
                </li>
                <li>
                  {/* <a href="JavaScript:Void(0);"><img src="../images/bell.png" /></a> */}
                </li>
                <li>
                  <a href="/settings"><img src={process.env.PUBLIC_URL + '/images/setting.png'} /></a>
                </li>

                <li class="account">
                  <div class="dropdown">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                      {/* <img src="../images/grp.png" /> &nbsp; {userName && userName} */}
                      <img src={photo} /> &nbsp; <span>{userName && userName}</span> 
                    </button>
                    <div class="dropdown-menu">
                      {/* <a class="dropdown-item" href="JavaScript:Void(0);">Account 1</a> */}
                      {/* <a class="dropdown-item" href="JavaScript:Void(0);">Account 2</a> */}
                      <a class="dropdown-item" href="JavaScript:Void(0);" onClick={()=>logOut(history, dispatch)}>Signout</a>
                    </div>
                  </div>
                </li>
                
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </nav>
  );
};

// Header.propTypes = {

// };

const condition = authUser => authUser && authUser.user.role_id === CF_USER;
export default withAuthorization(condition)(Header);