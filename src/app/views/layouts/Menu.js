import React, { useState, useEffect } from 'react';
// import PropTypes from 'prop-types';
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { selectUser, setMenu } from '../../actions/user/userSlice';


const Menu = props => {

  const user = useSelector(selectUser);
  const dispatch = useDispatch();
  const history = useHistory();

  const [currentMenu, setCurrentMenu] = useState("");

  useEffect(() => {
    console.log("Current menu is ===", user.menu);
    if (!user || !user.menu) {
      setCurrentMenu("home");
    } else if (user && user.menu) {
      switch (user.menu) {
        case "":
          setCurrentMenu("home");
          break;
        case "home":
          setCurrentMenu("home");
          break;
        case "activity":
          setCurrentMenu("activity");
          break;
        case "new":
          setCurrentMenu("new");
          break;
        case "dictionary":
          setCurrentMenu("dictionary");
          break;
        case "profile":
          setCurrentMenu("profile");
          break;
        case "favourite":
          setCurrentMenu("favourite");
          break;
        case "faq":
          setCurrentMenu("faq");
          break;
        case "invite":
          setCurrentMenu("invite");
          break;
        case "contact":
          setCurrentMenu("contact");
          break;
        case "money":
          setCurrentMenu("money");
          break;
        case "wallet":
          setCurrentMenu("wallet");
          break;
        case "about":
          setCurrentMenu("about");
          break;
        default:
          setCurrentMenu("home");
          break;
      }
    }
  }, []);

  useEffect(() => {
    if (user && user.menu) {
      setCurrentMenu(user.menu);
    }
  }, [user, setCurrentMenu]);

  // For changing menu - 
  const changeMenu = (menu) => {
    // console.log("change menus===", menu);
    dispatch(setMenu(menu));

    switch (menu) {
      case "home":
        redirectTo("/modules");
        break;
      case "activity":
        redirectTo("/activity");
        break;
      case "new":
        redirectTo();
        break;
      case "dictionary":
        redirectTo();
        break;
      case "profile":
        redirectTo("/editProfileGift");
        break;
      case "favourite":
        redirectTo();
        break;
      case "faq":
        redirectTo();
        break;
      case "invite":
        redirectTo("/invitation");
        break;
      case "contact":
        redirectTo();
        break;
      case "money":
        redirectTo();
        break;
      case "about":
        redirectTo();
        break;
      case "wallet":
        redirectTo("/modules");
      default:
        redirectTo("/modules");

    }

  }

  const redirectTo = (path) => {
    if (path && path != "") {
      history.push({
        pathname: path,
        state: {}
      })
    }
  }

  return (
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item m-auto">
            <a class="navbar-brand" href="#">
              <img src={process.env.PUBLIC_URL + '/images/logo.png'} />
            </a>
          </li>
          <li class="nav-item nav-toggle">
            <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
              <i class="bx bx-x d-block d-xl-none font-medium-4 toggle-icon"></i>
              <i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="bx-disc"></i>
            </a>
          </li>
        </ul>
      </div>

      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="">
          <li class="nav-item" class= {currentMenu=="home"? "active": ""}>
            <a href="JavaScript:void(0);" onClick={() => changeMenu("home")}>
              <i><img src={process.env.PUBLIC_URL + '/images/01.png'} /></i><span class="menu-title" >Home</span>
            </a>
          </li>
          <li class="nav-item" class= {currentMenu=="activity"? "active": ""}>
            <a href="JavaScript:void(0);" onClick={() => changeMenu("activity")}>
              <i><img src={process.env.PUBLIC_URL + '/images/02.png'} /></i><span class="menu-title">Activity</span>
            </a>
          </li>
          {/* <li class="nav-item" class= {currentMenu=="new"? "active": ""}>
            <a href="JavaScript:void(0);" onClick={() => changeMenu("new")} >
              <i><img src="../images/03.png" /></i><span class="menu-title">What's New</span>
            </a>
          </li> */}
          {/* <li class="nav-item" class= {currentMenu=="dictionary"? "active": ""}>
            <a href="JavaScript:void(0);" onClick={() => changeMenu("dictionary")}>
              <i><img src="../images/04.png" /></i><span class="menu-title">Dictionary</span>
            </a>
          </li> */}
          <li class="nav-item" class= {currentMenu=="profile"? "active": ""}>
            <a href="JavaScript:void(0);" onClick={() => changeMenu("profile")}>
              <i><img src={process.env.PUBLIC_URL + '/images/05.png'} /></i><span class="menu-title">Profile</span>
            </a>
          </li>
          {/* <li class="nav-item" class= {currentMenu=="favourite"? "active": ""}>
            <a href="JavaScript:void(0);" onClick={() => changeMenu("favourite")}>
              <i><img src="../images/06.png" /></i><span class="menu-title">Favourite</span>
            </a>
          </li> */}
          {/* <li class="nav-item" class= {currentMenu=="faq"? "active": ""}>
            <a href="JavaScript:Void(0);" onClick={() => changeMenu("faq")}>
              <i><img src="../images/07.png" /></i><span class="menu-title">FAQ</span>
            </a>
          </li> */}
          <li class="nav-item" class= {currentMenu=="invite"? "active": ""}>
            <a href="JavaScript:Void(0);" onClick={() => changeMenu("invite")}>
              <i><img src={process.env.PUBLIC_URL + '/images/08.png'} /></i><span class="menu-title">Invite Friends</span>
            </a>
          </li>
          {/* <li class="nav-item" class= {currentMenu=="contact"? "active": ""}>
            <a href="JavaScript:Void(0);" onClick={() => changeMenu("contact")}>
              <i><img src="../images/09.png" /></i><span class="menu-title">Contact Us</span>
            </a>
          </li> */}
          {/* <li class="nav-item" class= {currentMenu=="money"? "active": ""}>
            <a href="JavaScript:Void(0);" onClick={() => changeMenu("money")}>
              <i><img src="../images/10.png" /></i><span class="menu-title">Money Back Term</span>
            </a>
          </li>
          <li class="nav-item" class= {currentMenu=="about"? "active": ""}>
            <a href="JavaScript:Void(0);" onClick={() => changeMenu("about")}>
              <i><img src="../images/11.png" /></i><span class="menu-title">About Us</span>
            </a>
          </li>
          <li class="nav-item" class= {currentMenu=="wallet"? "active": ""}>
            <a href="JavaScript:Void(0);" onClick={() => changeMenu("wallet")}>
              <i><img src="../images/12.png" /></i><span class="menu-title">Wallet</span>
            </a>
          </li> */}
        </ul>
      </div>
    </div>
  );
};

// Menu.propTypes = {

// };

export default Menu;